-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 04 Lip 2020, 09:19
-- Wersja serwera: 10.4.8-MariaDB
-- Wersja PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `shop`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `carts`
--

CREATE TABLE `carts` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `carts`
--

INSERT INTO `carts` (`id`, `id_user`) VALUES
(75, 8);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `carts_done`
--

CREATE TABLE `carts_done` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `status` varchar(30) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `carts_done_positions`
--

CREATE TABLE `carts_done_positions` (
  `id` int(11) NOT NULL,
  `id_cart_done` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `size` varchar(2) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `carts_done_positions`
--

INSERT INTO `carts_done_positions` (`id`, `id_cart_done`, `id_item`, `quantity`, `size`) VALUES
(26, 15, 14, 3, 'S'),
(27, 15, 1, 1, 'S'),
(28, 15, 13, 1, 'M'),
(29, 15, 7, 3, 'XL'),
(30, 22, 14, 3, 'S'),
(31, 22, 7, 1, 'XS'),
(32, 23, 6, 99, 'XS'),
(33, 23, 15, 4, 'M'),
(34, 24, 13, 2, 'XS'),
(35, 24, 19, 1, 'M'),
(36, 24, 6, 1, 'S'),
(37, 15, 15, 1, 'XS'),
(38, 15, 15, 1, 'XS'),
(39, 15, 15, 1, 'XS'),
(40, 15, 15, 1, 'XS'),
(41, 15, 15, 1, 'XS'),
(42, 15, 15, 1, 'XS'),
(43, 27, 13, 1, 'S'),
(44, 27, 5, 1, 'S'),
(45, 27, 8, 1, 'XS'),
(46, 27, 5, 1, 'S');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `carts_positions`
--

CREATE TABLE `carts_positions` (
  `id` int(11) NOT NULL,
  `id_cart` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT 1,
  `size` varchar(2) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `carts_positions`
--

INSERT INTO `carts_positions` (`id`, `id_cart`, `id_item`, `quantity`, `size`) VALUES
(70, 75, 2, 1, 'S');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `name-pl` varchar(100) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `categories`
--

INSERT INTO `categories` (`id`, `name`, `name-pl`) VALUES
(1, 'trousers', 'spodnie'),
(2, 't-shirts', 'koszulki'),
(3, 'hats', 'czapki'),
(5, 'socks', 'skarpety');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `short` varchar(20) COLLATE utf8_polish_ci NOT NULL,
  `category` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `color` varchar(30) COLLATE utf8_polish_ci NOT NULL,
  `sizes` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `price` float NOT NULL,
  `price_new` float NOT NULL DEFAULT 0,
  `stock` int(11) NOT NULL,
  `gender` varchar(1) COLLATE utf8_polish_ci NOT NULL,
  `images` text COLLATE utf8_polish_ci NOT NULL,
  `special` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `items`
--

INSERT INTO `items` (`id`, `name`, `short`, `category`, `color`, `sizes`, `price`, `price_new`, `stock`, `gender`, `images`, `special`) VALUES
(1, 'FAYZA-NE - JOGG - Jeansy Relaxed Fit', 'JOGG - Jeansy', 'trousers', 'indigo', 'XS,S,M', 1149, 959.14, 32, 'F', '1.png,2.png,3.png,4.png,5.png', 1),
(2, 'TEPPHAR - Jeansy Skinny.', 'TEPPHAR - Jeansy', 'trousers', '080ac ', 'S,M,L', 677, 355, 35, 'M', '1.png,2.png,3.jpg', 1),
(5, 'WASHED STRUCTURE - Chinosy', '', 'trousers', 'beige', 'S,XL', 209, 0, 99, 'M', '1.png,TO222E04K-B11@8.jpg,TO222E04K-B11@9.webp.png,TO222E04K-B11@11.webp.png,TO222E04K-B11@12.webp.png', 0),
(6, 'HIGH WAISTED CAMO TROUSERS - Bojówki', '', 'trousers', 'grey', 'XS,S,M', 159, 0, 15, 'F', '1.png,2.png,3.png,4.png', 0),
(7, 'Adidas spodnie treningowe', '', 'trousers', 'scarlet', 'XS,S,M,L,XL', 199, 0, 99, 'F', '1.png,2.png,3.png,4.png', 0),
(8, 'GAP CITY - shorty', '', 'trousers', 'superlime', 'XS,S,M', 129, 77, 19, 'F', '1.png,2.png,3.png,4.png', 1),
(9, 'NMKERRY MARBLE - Legginsy', '', 'trousers', 'black/white', 'XS,S,M,XL', 84, 0, 99, 'F', '1.png,2.png,3.png,4.png,5.png', 1),
(11, 'HERITAGE - Spodnie treningowe', '', 'trousers', 'midnight navy', 'XS,S,M,L,XL', 199, 0, 99, 'M', '2.png,3.png,4.png,NI122E04Y-K13@4.webp.png', 0),
(12, 'Topman - spodnie materiałowe', '', 'trousers', 'red', 'S,M,L,XL', 169, 0, 99, 'M', '1.png,2.png,3.png,4.png,5.png', 0),
(13, 'CROPPED - Bluzka z długim rękawem', '', 't-shirts', 'ash pearl/black', 'XS,S,M', 269, 201, 33, 'F', '1.png,2.png,3.png,4.png,5.png', 1),
(14, 'ONYDANNA - Bluzka', '', 't-shirts', 'crème brûlée', 'XS,S,M', 139, 85, 37, 'F', '1.png,2.png,3.png,4.png,5.png', 1),
(15, 'NMLINEA - Bluzka z długim rękawem', '', 't-shirts', 'olivine', 'XS,S,M', 84, 0, 37, 'F', '1.png,2.png,3.png,4.png,5.png', 0),
(16, 'VMFLORENCE SINGLET - Top', '', 't-shirts', 'port royale', 'XS,M', 119, 0, 37, 'F', '1.png,2.png,3.png,4.png,5.png', 0),
(17, 'THE PERFECT TEE - T-shirt z nadrukiem', '', 't-shirts', 'white/red', 'XS,S,M,L,XL', 99, 72, 34, 'F', '1.png,2.png,3.png,4.png,5.png', 0),
(18, 'REVEAL YOUR VOICE LONGSLEEVE - Bluzka z długim rękawem', '', 't-shirts', 'black', 'S,M,L', 179, 125, 43, 'M', '1.png,2.png,3.png,4.png', 0),
(19, 'CREW - Bluzka z długim rękawem', '', 't-shirts', 'carbon', 'S,M,L', 489, 0, 34, 'M', '1.png,2.png,3.png,4.png', 0),
(20, 'HUGO DICAGOLINO - T-shirt z nadrukiem', '', 't-shirts', 'black', 'S,M,XL', 254, 0, 34, 'M', '1.png,2.png,3.png,4.png', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `hash` mediumtext COLLATE utf8_polish_ci NOT NULL,
  `salt` mediumtext COLLATE utf8_polish_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `login`, `email`, `hash`, `salt`, `type`) VALUES
(8, 'test', 'test@test.test', 'OdXFfnx35QamKOodg8dHhInX0xqf0mYv2aQ2n0IKcOY=', 'ex6tJw47Oc+r+Xq/2jE8XEV968g7extfERehC1MpEnQ=', 'admin');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_user` (`id_user`);

--
-- Indeksy dla tabeli `carts_done`
--
ALTER TABLE `carts_done`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `carts_done_positions`
--
ALTER TABLE `carts_done_positions`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `carts_positions`
--
ALTER TABLE `carts_positions`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indeksy dla tabeli `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `login` (`login`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT dla tabeli `carts_done`
--
ALTER TABLE `carts_done`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT dla tabeli `carts_done_positions`
--
ALTER TABLE `carts_done_positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT dla tabeli `carts_positions`
--
ALTER TABLE `carts_positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT dla tabeli `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT dla tabeli `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
