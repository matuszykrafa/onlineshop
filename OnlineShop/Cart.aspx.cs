﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineShop
{
    public partial class Cart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                Generator.GenerateHelloLabel(lLoginButtons, Session["user"].ToString());
                Generator.GenerateCart(panelCart, Session["user_id"].ToString());
            }
            else
            {
                Generator.GenerateLoginButtons(lLoginButtons);
                Response.Redirect("/Login.aspx");
            }
            Generator.GenerateDropdownCategory(PanelDropdownMale, Resources.Resource.lMale);
            Generator.GenerateDropdownCategory(PanelDropdownFemale, Resources.Resource.lFemale);
            
        }

        protected void lbtnLanguage_Click(object sender, EventArgs e)
        {
            string option = lbtnLanguage.Text.ToString();
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(option);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(option);
            Session["culture"] = option;
            Response.Redirect(Request.Url.PathAndQuery);
        }

        public static void TbQuantity_Click(object sender, EventArgs e, string id)
        {
            string value = ((TextBox)sender).Text;
            try
            {
                if (Int32.Parse(value) > 0 && Int32.Parse(value) < 100)
                {
                    Database.ChangeQuantity(id, value);
                }
                else
                {
                    Database.ChangeQuantity(id, "1");
                    ((TextBox)sender).Text = "1";
                }
            }
            catch
            {
                Database.ChangeQuantity(id, "1");
                ((TextBox)sender).Text = "1";
            }
        }

        public static void btDeletePosition_Click(object sender, EventArgs e, string id)
        {
            int result = Database.DeleteCartPosition(id);
            if (result == 1) HttpContext.Current.Response.Redirect(HttpContext.Current.Request.Url.PathAndQuery);
        }

        protected override void InitializeCulture()
        {
            if (Session["culture"] != null)
            {
                UICulture = Session["culture"].ToString();
                Culture = Session["culture"].ToString();
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Session["culture"].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["culture"].ToString());
            }
            base.InitializeCulture();
        }

        internal static void btOrder_Click(object sender, EventArgs e, List<cartPosition> carts, string user_id)
        {
            int order_id = Database.Order(user_id);
            if(order_id != -1)
            {
                foreach (cartPosition cart in carts)
                {
                    Database.addToCartDone(user_id, cart.id_item,cart.size,cart.quantity);
                }
                Database.DeleteCart(user_id);
                MailMessage message = new MailMessage("clothesland2@gmail.com", HttpContext.Current.Session["user_email"].ToString());
                message.Subject = "Order: " + order_id;
                message.Body = "Twoje zamówienie przyjęte. <br> Numer twojego zamówienia to: " + order_id + ". <br> Status: <b>" + Resources.Resource.lMakePlaced + "</b><br><br> Twoje zamówienie:<br><table><tr><th>Nazwa</th><th>Ilość</th><th>Rozmiar</th><th>Cena jednostkowa</th><th>Łączna cena</th></tr>";
                float suma = 0;
                foreach (cartPosition cart in carts)
                {
                    message.Body += "<tr><td>"+cart.name+ "</td><td>" + cart.quantity + "</td><td>" + cart.size + "</td><td>" + cart.price + "</td><td>$" + (float.Parse(cart.price)*float.Parse(cart.quantity)).ToString() + "</td></tr>";
                    suma += float.Parse(cart.price) * float.Parse(cart.quantity);
                }

                message.Body += "</table><p>Suma: <b>$" + suma.ToString() + "</b></p><br><p>Clothesland</p>";
                
                message.IsBodyHtml = true;
                SmtpClient client = new SmtpClient("smtp.gmail.com");
                client.UseDefaultCredentials = false;
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential("clothesland2@gmail.com","mamatata1");
                try
                {
                    client.Send(message);
                    HttpContext.Current.Response.Redirect("/Cart.aspx");
                }
                catch
                {
                    Debug.WriteLine("Błąd przy wysyłaniu maila");
                }
            }
        }
    }
}