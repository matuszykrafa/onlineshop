﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineShop
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                Response.Redirect("/");
            }
            else
            {
                Generator.GenerateLoginButtons(lLoginButtons);
            }
            Generator.GenerateDropdownCategory(PanelDropdownMale, Resources.Resource.lMale);
            Generator.GenerateDropdownCategory(PanelDropdownFemale, Resources.Resource.lFemale);
        }

        protected void lbtnLanguage_Click(object sender, EventArgs e)
        {
            string option = lbtnLanguage.Text.ToString();
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(option);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(option);
            Session["culture"] = option;
            Response.Redirect(Request.Url.LocalPath);
        }

        protected override void InitializeCulture()
        {
            if (Session["culture"] != null)
            {
                UICulture = Session["culture"].ToString();
                Culture = Session["culture"].ToString();
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Session["culture"].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["culture"].ToString());
            }
            base.InitializeCulture();
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                dynamic user = Database.Login(tbUsername.Text.ToString(), tbPassword.Text.ToString());
                try
                {
                    string message = "";
                    if (user == 0)
                    {
                        message = Resources.Resource.loginProblem.ToString();
                    }
                    else if(user == -1)
                    {
                        message = Resources.Resource.databaseProblem.ToString();
                    }
                    string alert = "<script language =\"javascript\" type=\"text/javascript\">alert('" + message + "');</script>";
                    Response.Write(alert);
                }
                catch
                {
                    Session["user"] = user.user;
                    Session["user_type"] = user.type;
                    Session["user_email"] = user.email;
                    Session["user_id"] = user.id;
                    Response.Redirect("/");
                }
            }
        }
    }
}