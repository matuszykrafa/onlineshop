﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace OnlineShop
{
    public class Database
    {
        static MySqlConnection connect()
        {
            string myconnection =
               "SERVER=127.0.0.1;" +
               "DATABASE=shop;" +
               "UID=root;" +
               "PASSWORD=;";

            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                Console.WriteLine("Connected");
                return connection;
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                Console.WriteLine("Error");
            }
            return null;
        }

        public static int Insert(string query)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;

            MySqlCommand command = conn.CreateCommand();

            command.CommandText = query;
            try
            {
                command.ExecuteNonQuery();
            }
            catch
            {
                return 0;
            }
            return 1;
        }


        public static dynamic Login(string username, string password)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;
            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "SELECT `id`, `login`,`email`,`hash`,`salt`,`type` FROM `users` WHERE `login`='" + username + "'";
            MySqlDataReader reader = command.ExecuteReader();
            User user = new User();
            while (reader.Read())
            {
                user.id = reader.GetString("id");
                user.user = reader.GetString("login");
                user.email = reader.GetString("email");
                user.password = reader.GetString("hash");
                user.salt = reader.GetString("salt");
                user.type = reader.GetString("type");
            }
            conn.Close();
            try
            {
                var hash = new Rfc2898DeriveBytes(password, Convert.FromBase64String(user.salt), 10000).GetBytes(32);
                if (user.password == Convert.ToBase64String(hash))
                {
                    return user;
                }
                else
                {
                    return 0;
                }
            }
            catch
            {
                return 0;
            }
        }

        internal static int DeleteCartPosition(string id)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;

            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "DELETE FROM `carts_positions` WHERE `id`= '" + id + "'";
            try
            {
                command.ExecuteNonQuery();
            }
            catch
            {
                return 0;
            }
            return 1;
        }

        internal static int Order(string user_id)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;

            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "INSERT INTO `carts_done` (`id`, `id_user`,`status`) VALUES (NULL, '" + user_id + "', 'Placed')";
            try
            {
                command.ExecuteNonQuery();
            }
            catch
            {
                conn.Close();
                return -1;
            }
            int id = -1;
            command.CommandText = "SELECT `id` FROM `carts_done` WHERE `id_user`= '" + user_id + "'";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                id = Int32.Parse(reader.GetString("id"));
            }
            conn.Close();
            return id;
        }

        internal static int DeleteCart(string user_id)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;

            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "DELETE FROM `carts_positions` WHERE `id_cart`=(SELECT `id` FROM `carts` WHERE `carts`.`id_user` = " + user_id + ")";
            try
            {
                command.ExecuteNonQuery();
            }
            catch
            {
                return 0;
            }
            return 1;
        }

        internal static int ChangeQuantity(string id, string value)
        {

            MySqlConnection conn = connect();
            if (conn == null) return -1;

            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "UPDATE `carts_positions` SET `quantity`= '" + value + "' WHERE `id`= '" + id + "'";
            try
            {
                command.ExecuteNonQuery();
            }
            catch
            {
                return 0;
            }
            return 1;
        }

        internal static dynamic getCartPositions(string user_id)
        {
            MySqlConnection conn = connect();
            List<cartPosition> carts = new List<cartPosition>();
            if (conn != null)
            {
                MySqlCommand command = conn.CreateCommand();

                command.CommandText = "SELECT `carts_positions`.`id`, `id_cart`, `id_item`, `name`, IF(`price_new`=0,`price`,`price_new`) AS price,`quantity`, `size` FROM `carts_positions` INNER JOIN `items` ON `carts_positions`.`id_item`=`items`.`id` WHERE `id_cart`=(SELECT `id` FROM `carts` WHERE `id_user`=" + user_id + ")";
                MySqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    cartPosition pos = new cartPosition
                    {
                        id = reader.GetString("id"),
                        id_cart = reader.GetString("id_cart"),
                        id_item = reader.GetString("id_item"),
                        quantity = reader.GetString("quantity"),
                        size = reader.GetString("size"),
                        name = reader.GetString("name"),
                        price = reader.GetString("price")
                    };
                    carts.Add(pos);
                }
                conn.Close();
            }

            return carts;
        }

        internal static int createCart(string user_id)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;

            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "INSERT INTO `carts` (`id`, `id_user`) VALUES (NULL, '" + user_id + "')";
            try
            {
                command.ExecuteNonQuery();
            }
            catch
            {
                conn.Close();
                return 0;
            }
            conn.Close();
            return 1;
        }
        internal static int addToCart(string user_id, string item_id, string size, string quantity)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;

            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "INSERT INTO `carts_positions` (`id`, `id_cart`, `id_item`, `quantity`, `size`) VALUES (NULL, (SELECT `id` FROM `carts` WHERE `id_user`='" + user_id + "' LIMIT 1), '" + item_id + "', '" + quantity + "', '" + size + "')";
            try
            {
                command.ExecuteNonQuery();
            }
            catch
            {
                conn.Close();
                return 0;
            }
            conn.Close();
            return 1;
        }

        internal static int addToCartDone(string user_id, string item_id, string size, string quantity)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;

            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "INSERT INTO `carts_done_positions` (`id`, `id_cart_done`, `id_item`, `quantity`, `size`) VALUES (NULL, (SELECT `id` FROM `carts_done` WHERE `id_user`='" + user_id + "' LIMIT 1), '" + item_id + "', '" + quantity + "', '" + size + "')";
            try
            {
                command.ExecuteNonQuery();
            }
            catch
            {
                conn.Close();
                return 0;
            }
            conn.Close();
            return 1;
        }

        internal static dynamic GetSpecialOffers()
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;
            Debug.WriteLine("Błąd połączenia z bazą");
            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "SELECT * FROM `items` WHERE `special`=1";
            MySqlDataReader reader = command.ExecuteReader();
            List<Item> items = new List<Item>();
            while (reader.Read())
            {
                Item item = new Item();
                item.id = reader.GetString("id");
                item.name = reader.GetString("name");
                item.category = reader.GetString("name");
                item.color = reader.GetString("name");
                item.sizesString = reader.GetString("name");
                item.price = reader.GetInt32("price");
                item.price_new = reader.GetInt32("price_new");
                item.stock = reader.GetInt32("stock");
                item.gender = reader.GetString("gender");
                item.imagesString = reader.GetString("images");
                item.images = reader.GetString("images").Split(',');
                items.Add(item);
            }
            conn.Close();
            return items;
        }

        internal static dynamic GetTrendingOffers(int cols)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;
            Debug.WriteLine("Błąd połączenia z bazą");
            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "SELECT COUNT(`carts_done_positions`.`id_item`) AS ile, `items`.`id`, `items`.`name`, `items`.`short`, `items`.`category`, `items`.`color`, `items`.`sizes`, `items`.`price`, `items`.`price_new`, `items`.`stock`, `items`.`gender`, `items`.`images`, `items`.`special` FROM `carts_done_positions` INNER JOIN `items` ON `carts_done_positions`.`id_item`=`items`.`id` WHERE 1 GROUP BY `id_item` ORDER BY ile DESC LIMIT " + cols.ToString();
            MySqlDataReader reader = command.ExecuteReader();
            List<Item> items = new List<Item>();
            while (reader.Read())
            {
                Item item = new Item();
                item.id = reader.GetString("id");
                item.name = reader.GetString("name");
                item.category = reader.GetString("name");
                item.color = reader.GetString("name");
                item.sizesString = reader.GetString("name");
                item.price = reader.GetInt32("price");
                item.price_new = reader.GetInt32("price_new");
                item.stock = reader.GetInt32("stock");
                item.gender = reader.GetString("gender");
                item.imagesString = reader.GetString("images");
                item.images = reader.GetString("images").Split(',');
                items.Add(item);
            }
            conn.Close();
            return items;
        }

        internal static dynamic GetOffers(string sex, string category)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;
            Debug.WriteLine("Błąd połączenia z bazą");
            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "SELECT * FROM `items` WHERE `category`='" + category + "' AND gender='" + sex + "'";
            MySqlDataReader reader = command.ExecuteReader();
            List<Item> items = new List<Item>();
            while (reader.Read())
            {
                Item item = new Item();
                item.id = reader.GetString("id");
                item.name = reader.GetString("name");
                item.category = reader.GetString("name");
                item.color = reader.GetString("name");
                item.sizesString = reader.GetString("name");
                item.price = reader.GetInt32("price");
                item.price_new = reader.GetInt32("price_new");
                item.stock = reader.GetInt32("stock");
                item.gender = reader.GetString("gender");
                item.imagesString = reader.GetString("images");
                item.images = reader.GetString("images").Split(',');
                items.Add(item);
            }
            conn.Close();
            return items;
        }

        public static dynamic GetUser(string username)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;
            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "SELECT `login`,`email`,`hash`,`salt`,`type` FROM `users` WHERE `login`='" + username + "'";
            MySqlDataReader reader = command.ExecuteReader();
            User user = new User();
            while (reader.Read())
            {
                user.user = reader.GetString("login");
                user.email = reader.GetString("email");
                user.password = reader.GetString("hash");
                user.salt = reader.GetString("salt");
                user.type = reader.GetString("type");
            }
            conn.Close();
            return user;
        }

        internal static string GetNewID()
        {
            MySqlConnection conn = connect();
            if (conn == null) return "error";

            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "SELECT `id` FROM `items` WHERE 1 GROUP BY id DESC LIMIT 1";
            try
            {
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    return reader.GetString("id");
                }
            }
            catch
            {
                return "error";
            }
            return "error";
        }

        internal static int deleteUser(string id)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;

            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "DELETE FROM `users` WHERE `users`.`id` = " + id; try
            {
                command.ExecuteNonQuery();
            }
            catch
            {
                conn.Close();
                return 0;
            }
            conn.Close();
            return 1;
        }

        public static dynamic ChangePassword(User user)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;

            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "UPDATE `users` SET `hash` = '" + user.password + "', `salt` = '" + user.salt + "' WHERE `users`.`login` = '" + user.user + "'";
            try
            {
                command.ExecuteNonQuery();
            }
            catch
            {
                conn.Close();
                return 0;
            }
            conn.Close();
            return 1;
        }

        internal static int changeOrderStatus(string id, string status)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;

            MySqlCommand command = conn.CreateCommand();
            command.CommandText = "UPDATE `carts_done` SET `status` = '" + status + "' WHERE `carts_done`.`id` = " + id;
            try
            {
                command.ExecuteNonQuery();
            }
            catch
            {
                conn.Close();
                return 0;
            }
            conn.Close();
            return 1;
        }

        internal static string getEmailFromOrder(string id)
        {

            MySqlConnection conn = connect();
            if (conn == null) return "err";
            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "SELECT `email` FROM `users` INNER JOIN `carts_done` ON `carts_done`.`id_user` = `users`.`id` WHERE `carts_done`.`id`= " + id + " LIMIT 1";
            MySqlDataReader reader = command.ExecuteReader();
            string email = "";
            while (reader.Read())
            {
                email = reader.GetString("email");
            }
            conn.Close();
            return email;

        }

        internal static int changeUserType(string id, string type)
        {

            MySqlConnection conn = connect();
            if (conn == null) return -1;

            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "UPDATE `users` SET `type` = '" + type + "' WHERE `users`.`id` = " + id;
            try
            {
                command.ExecuteNonQuery();
            }
            catch
            {
                conn.Close();
                return 0;
            }
            conn.Close();
            return 1;
        }

        public static int AddItem(Item item)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;

            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "INSERT INTO `items` (`id`, `name`, `short`, `category`, `color`, `sizes`, `price`, `price_new`, `stock`, `gender`, `images`) " +
                "VALUES (" + item.id + ", '" + item.name + "', '', '" + item.category + "', '" + item.color + "', '" + item.sizesString + "', " + item.price.ToString() + ", " +
                "" + item.price_new.ToString() + ", " + item.stock.ToString() + ", '" + item.gender + "', '" + item.imagesString + "')";
            try
            {
                command.ExecuteNonQuery();
            }
            catch
            {
                conn.Close();
                return 0;
            }
            conn.Close();
            return 1;
        }

        internal static int changeSpecial(string id, string value)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;

            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "UPDATE `items` SET `special` = '"+value+"' WHERE `items`.`id` = " + id;
            try
            {
                command.ExecuteNonQuery();
            }
            catch
            {
                conn.Close();
                return 0;
            }
            conn.Close();
            return 1;
        }

        

        public static int DeleteItem(string id)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;

            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "DELETE FROM `items` WHERE `items`.`id` = " + id;
            try
            {
                command.ExecuteNonQuery();
            }
            catch
            {
                conn.Close();
                return 0;
            }
            conn.Close();
            return 1;
        }

        public static int UpdateItem(Item item)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;

            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "UPDATE `items` " +
                "SET `name` = '" + item.name + "', `category` = '" + item.category + "', `color` = '" + item.color + "', `sizes` = '" + item.sizesString + "', " +
                "`price` = " + item.price + ", `price_new` = " + item.price_new + ", `stock` = " + item.stock + ", `gender` = '" + item.gender + "', `images` = '" + item.imagesString + "' " +
                "WHERE `items`.`id` = " + item.id;
            try
            {
                command.ExecuteNonQuery();
            }
            catch
            {
                conn.Close();
                return 0;
            }
            conn.Close();
            return 1;
        }

        internal static dynamic SelectItem(string id)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;
            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "SELECT `id`, `name`, `short`, `category`, `color`,`sizes`, `price`, `price_new`, `stock`,`gender`, `images` FROM `items` WHERE id = " + id + "";
            MySqlDataReader reader = command.ExecuteReader();
            Item item = new Item();
            while (reader.Read())
            {
                item.id = reader.GetString("id");
                item.name = reader.GetString("name");
                item.category = reader.GetString("category");
                item.color = reader.GetString("color");
                string sizes = reader.GetString("sizes");
                char[] separator = { ',' };
                item.sizes = sizes.Split(separator);
                item.price = reader.GetInt32("price");
                item.price_new = reader.GetInt32("price_new");
                item.stock = reader.GetInt32("stock");
                item.gender = reader.GetString("gender");
                string images = reader.GetString("images");
                item.images = images.Split(separator);
            }
            conn.Close();
            try
            {
                return item;
            }
            catch
            {
                return 0;
            }
        }

        public static int AddCategory(string name, string namePL)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;

            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "INSERT INTO `categories` (`id`, `name`, `name-pl`) VALUES (NULL, '" + name + "', '" + namePL + "')";
            try
            {
                command.ExecuteNonQuery();
            }
            catch
            {
                conn.Close();
                return 0;
            }
            conn.Close();
            return 1;
        }

        public static int DeleteCategory(string id)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;

            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "DELETE FROM `categories` WHERE `categories`.`id` = " + id;
            try
            {
                command.ExecuteNonQuery();
            }
            catch
            {
                conn.Close();
                return 0;
            }
            conn.Close();
            return 1;
        }

        public static int UpdateCategory(string id, string name, string namePL)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;

            MySqlCommand command = conn.CreateCommand();


            command.CommandText = "UPDATE `items` SET `category` = '" + name + "' WHERE `category` = (SELECT `categories`.`name` FROM `categories` WHERE `categories`.`id` = " + id + " LIMIT 1)";
            try
            {
                command.ExecuteNonQuery();
            }
            catch
            {
                conn.Close();
                return 0;
            }
            command.CommandText = "UPDATE `categories` SET `name` = '" + name + "', `name-pl` = '" + namePL + "' WHERE `categories`.`id` = " + id;
            try
            {
                command.ExecuteNonQuery();
            }
            catch
            {
                conn.Close();
                return 0;
            }
            conn.Close();
            return 1;
        }

        internal static dynamic SelectCategories()
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;
            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "SELECT DISTINCT `name`,`name-pl` FROM `categories`";
            MySqlDataReader reader = command.ExecuteReader();

            List<Category> categories = new List<Category>();
            while (reader.Read())
            {
                Category cat = new Category();
                cat.name = reader.GetString("name");
                cat.namePL = reader.GetString("name-pl");
                categories.Add(cat);
            }
            conn.Close();
            return categories;
        }

        internal static dynamic SelectCategory(string category)
        {
            MySqlConnection conn = connect();
            if (conn == null) return -1;
            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "SELECT DISTINCT `name`,`name-pl` FROM `categories` WHERE `name`='" + category + "'";
            MySqlDataReader reader = command.ExecuteReader();
            Category cat = new Category();
            while (reader.Read())
            {
                cat.name = reader.GetString("name");
                cat.namePL = reader.GetString("name-pl");
            }
            conn.Close();
            return cat;
        }
    }
}