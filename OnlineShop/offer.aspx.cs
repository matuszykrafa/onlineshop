﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineShop
{
    public partial class Offer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                Generator.GenerateHelloLabel(lLoginButtons, Session["user"].ToString());
            }
            else
            {
                Generator.GenerateLoginButtons(lLoginButtons);
            }
            string id = HttpUtility.ParseQueryString(Request.Url.Query).Get("id");
            if (id != null)
            {
                try
                {
                    Generator.GenerateDropdownCategory(PanelDropdownMale, Resources.Resource.lMale);
                    Generator.GenerateDropdownCategory(PanelDropdownFemale, Resources.Resource.lFemale);
                    dynamic item = Generator.GeneratePanelImagesCount(panelImagesCount, id);
                    Generator.GeneratePanelImages(panelImages, id, item);
                    Generator.GeneratePanelOfferName(panelOfferName, id, item);
                    Generator.GeneratePanelOfferSize(panelOfferSize, id, item);
                }
                catch
                {
                    Response.Redirect("/");
                }
            }
            else
            {
                Response.Redirect("/");
            }
        }

        protected void lbtnLanguage_Click(object sender, EventArgs e)
        {
            string option = lbtnLanguage.Text.ToString();
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(option);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(option);
            Session["culture"] = option;
            Response.Redirect(Request.Url.PathAndQuery);
        }

        protected void lbtnAddToCart_Click(object sender, EventArgs e)
        {
            if(Session["user"] != null)
            {
                Database.createCart(Session["user_id"].ToString());

                Database.addToCart(Session["user_id"].ToString(), HttpUtility.ParseQueryString(Request.Url.Query).Get("id"), Request.Form["selectSize"], tbQuantity.Text.ToString());

                Response.Redirect("/Cart.aspx");
            }
            else
            {
                Response.Redirect("/Login.aspx");
            }
        }

        protected override void InitializeCulture()
        {
            if (Session["culture"] != null)
            {
                UICulture = Session["culture"].ToString();
                Culture = Session["culture"].ToString();
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Session["culture"].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["culture"].ToString());
            }
            base.InitializeCulture();
        }
    }
}