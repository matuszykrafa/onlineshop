﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineShop
{
    public class cartPosition
    {
        public string id { get; set; }
        public string id_cart { get; set; }
        public string id_item { get; set; }
        public string quantity { get; set; }
        public string size { get; set; }
        public string name { get; set; }
        public string price { get; set; }
    }
}