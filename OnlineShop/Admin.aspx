﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="OnlineShop.Admin" Culture="auto:en-US" UICulture="auto" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="assets/favicon.ico">
    <title>Clothesland</title>
    <script src="js/jquery-3.4.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid website bg-light">
            <header class="container">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
                        aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <img class="logo" src="assets/logo2.png" alt="" />Clothesland</a>

                    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li class="nav-item dropdown">
                            <asp:Panel runat="server" ID="PanelDropdownMale">
                                
                            </asp:Panel>
                        </li>
                        <li class="nav-item dropdown">
                            <asp:Panel runat="server" ID="PanelDropdownFemale">
                                
                            </asp:Panel>
                        </li>
                             <li class="nav-item">
                                <a class="nav-link" href="/Contact.aspx">
                                    <asp:Label ID="Label74" runat="server" Text="<%$Resources: Resource, lContact %>"></asp:Label></a>
                            </li>
                    </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <div class="dropdown-divider"></div>
                            </li>
                            <li>
                                <a class="nav-link" href="/Cart.aspx">
                                    <asp:Label ID="Label73" runat="server" Text="<%$Resources: Resource, lCart %>"></asp:Label></a>
                            </li>
                            <asp:Label ID="lLoginButtons" runat="server" CssClass="nav navbar-nav navbar-right"></asp:Label>
                            <li>
                                <div class="dropdown-divider"></div>
                            </li>

                            <li>
                                <asp:LinkButton ID="lbtnLanguage" runat="server" CssClass="nav-link" Text="<%$ Resources:Resource, lLang %>" OnClick="lbtnLanguage_Click" CausesValidation="False"></asp:LinkButton></li>
                        </ul>
                    </div>
                </nav>
            </header>
            <div id="content" class="container">
                <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:shopConnectionString %>' ProviderName='<%$ ConnectionStrings:shopConnectionString.ProviderName %>' SelectCommand="SELECT DISTINCT name, id FROM categories"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString='<%$ ConnectionStrings:shopConnectionString %>' ProviderName='<%$ ConnectionStrings:shopConnectionString.ProviderName %>' SelectCommand="SELECT id, name FROM items"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString='<%$ ConnectionStrings:shopConnectionString %>' ProviderName='<%$ ConnectionStrings:shopConnectionString.ProviderName %>' SelectCommand="SELECT `id`, CONCAT(`name`, ' (' ,`name-pl`, ')') AS names, `name` FROM categories"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString='<%$ ConnectionStrings:shopConnectionString %>' ProviderName='<%$ ConnectionStrings:shopConnectionString.ProviderName %>' SelectCommand="SELECT CONCAT(name, ' (', id, ') ', IF(special, '(Special)', '')) AS nameID, id, special FROM items"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSoutceUsers" runat="server" ConnectionString='<%$ ConnectionStrings:shopConnectionString %>' ProviderName='<%$ ConnectionStrings:shopConnectionString.ProviderName %>' SelectCommand="SELECT CONCAT(login, ' (', type, ')') AS loginType, id FROM users"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSourceOrders" runat="server" ConnectionString='<%$ ConnectionStrings:shopConnectionString %>' ProviderName='<%$ ConnectionStrings:shopConnectionString.ProviderName %>' SelectCommand="SELECT id, CONCAT('id:',id, '; login:',(SELECT login FROM `users` WHERE `users`.`id`=`carts_done`.`id_user`), ' (',`status`,')') AS name,( SELECT login FROM users WHERE `users`.id = id_user ) AS user, `carts_done`.`id_user`, STATUS FROM carts_done"></asp:SqlDataSource>


                <p class="display-4 mt-3">
                    <asp:Label ID="Label24" runat="server" Text="<%$ Resources:Resource, lCategory %>"></asp:Label>
                </p>
                <div class="row">
                    <div class="col-md-8">
                        <asp:ListBox CssClass="custom-select m-1" ID="listBoxCategories" runat="server" DataSourceID="SqlDataSource3" DataTextField="names" DataValueField="id"></asp:ListBox>
                        <button type="button" class="btn btn-primary m-1" data-toggle="modal" data-target="#updateCategory">
                            <asp:Label ID="Label17" runat="server" Text="<%$ Resources:Resource, lUpdateCategory %>"></asp:Label></button>
                        <button type="button" class="btn btn-danger m-1" data-toggle="modal" data-target="#deleteCategory">
                            <asp:Label ID="Label26" runat="server" Text="<%$ Resources:Resource, lDeleteCategory %>"></asp:Label></button>
                    </div>
                    <div class="col-md">
                        <button type="button" class="btn btn-primary m-1" data-toggle="modal" data-target="#addCategory">
                            <asp:Label ID="Label27" runat="server" Text="<%$ Resources:Resource, lAddCategory %>"></asp:Label></button>
                    </div>
                    <asp:Panel ID="panelAddCategory" runat="server">
                        <div class="modal fade" id="addCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <asp:Label ID="Label21" runat="server" Text="<%$ Resources:Resource, lAddCategory %>" CssClass="modal-title h5"></asp:Label>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div>
                                            <div class="mb-3">
                                                <label for="validationTextarea">
                                                    <asp:Label ID="Label22" runat="server" Text="<%$ Resources:Resource, lName %>"></asp:Label>
                                                </label>
                                                <asp:TextBox CssClass="form-control" ID="tbCategoryName" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ValidationGroup="addCategory" ID="RequiredFieldValidator8" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbCategoryName" CssClass="invalid-feedback2">
                                                    <asp:Label ID="Label23" runat="server" Text="<%$ Resources:Resource, lInvalidName %>"></asp:Label>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                            <div class="mb-3">
                                                <label for="validationTextarea">
                                                    <asp:Label ID="Label30" runat="server" Text="<%$ Resources:Resource, lNamePL %>"></asp:Label>
                                                </label>
                                                <asp:TextBox CssClass="form-control" ID="tbCategoryNamePL" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ValidationGroup="addCategory" ID="RequiredFieldValidator9" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbCategoryNamePL" CssClass="invalid-feedback2">
                                                    <asp:Label ID="Label31" runat="server" Text="<%$ Resources:Resource, lInvalidNamePL %>"></asp:Label>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <asp:LinkButton ValidationGroup="addCategory" ID="btnAddCategory" runat="server" CssClass="btn btn-primary" Text="<%$ Resources:Resource, lAddCategory %>" OnClick="btnAddCategory_Click"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panelUpdateCategory" runat="server">
                        <div class="modal fade" id="updateCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <asp:Label ID="Label18" runat="server" Text="<%$ Resources:Resource, lUpdateCategoryText %>" CssClass="modal-title h5"></asp:Label>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div>
                                            <div class="mb-3">
                                                <label for="validationTextarea">
                                                    <asp:Label ID="Label19" runat="server" Text="<%$ Resources:Resource, lName %>"></asp:Label>
                                                </label>
                                                <asp:TextBox CssClass="form-control" ID="tbUpdateCategory" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ValidationGroup="updateCategory" ID="RequiredFieldValidator4" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbUpdateCategory" CssClass="invalid-feedback2">
                                                    <asp:Label ID="Label20" runat="server" Text="<%$ Resources:Resource, lInvalidName %>"></asp:Label>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                            <div class="mb-3">
                                                <label for="validationTextarea">
                                                    <asp:Label ID="Label37" runat="server" Text="<%$ Resources:Resource, lNamePL %>"></asp:Label>
                                                </label>
                                                <asp:TextBox CssClass="form-control" ID="tbUpdateCategoryPolish" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ValidationGroup="updateCategory" ID="RequiredFieldValidator5" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbUpdateCategoryPolish" CssClass="invalid-feedback2">
                                                    <asp:Label ID="Label38" runat="server" Text="<%$ Resources:Resource, lInvalidNamePL %>"></asp:Label>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <asp:LinkButton ValidationGroup="updateCategory" ID="lbtnUpdateCategory" runat="server" CssClass="btn btn-primary" Text="<%$ Resources:Resource, lUpdateCategory %>" OnClick="lbtnUpdateCategory_Click"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panelDeleteCategory" runat="server">
                        <!-- Modal -->
                        <div class="modal fade" id="deleteCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <asp:Label CssClass="modal-title" ID="Label16" runat="server" Text="<%$ Resources:Resource,lDeleteCategory %>"></asp:Label>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lDeleteCategoryText %>"></asp:Label>
                                        <asp:Label ID="lSelectedCategory" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, lClose %>"></asp:Label></button>
                                        <asp:LinkButton CssClass="btn btn-danger" ID="lbtnDeleteCategory" runat="server" OnClick="lbtnDeleteCategory_Click">
                                            <asp:Label ID="Label34" runat="server" Text="<%$ Resources:Resource, lDeleteCategory %>"></asp:Label>
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>

                <p class="display-4 mt-3">
                    <asp:Label ID="Label25" runat="server" Text="<%$ Resources:Resource, lItems %>"></asp:Label>
                </p>
                <div class="row">
                    <div class="col-md-8">
                        <asp:ListBox CssClass="custom-select m-1" ID="listBoxItems" runat="server" DataSourceID="SqlDataSource4" DataTextField="nameID" DataValueField="id"></asp:ListBox>
                        <button type="button" class="btn btn-primary m-1" data-toggle="modal" data-target="#updateItem">
                            <asp:Label ID="Label52" runat="server" Text="<%$ Resources:Resource, lUpdateItem %>"></asp:Label></button>
                        <button type="button" class="btn btn-danger m-1" data-toggle="modal" data-target="#deleteItem">
                            <asp:Label ID="Label28" runat="server" Text="<%$ Resources:Resource, lDeleteItem %>"></asp:Label></button>
                        <asp:LinkButton CssClass="btn btn-primary m-1" ID="LinkButton9" runat="server" OnClick="lbtnMakeSpecial_Click" Text="<%$ Resources: Resource, lMakeSpecial %>"></asp:LinkButton>
                        <asp:LinkButton CssClass="btn btn-primary m-1" ID="LinkButton10" runat="server" OnClick="lbtnMakeNormal_Click" Text="<%$ Resources: Resource, lMakeNormal %>"></asp:LinkButton>
                    </div>
                    <div class="col-md">
                        <button type="button" class="btn btn-primary m-1" data-toggle="modal" data-target="#addItem">
                            <asp:Label ID="Label29" runat="server" Text="<%$ Resources:Resource, lAddItem %>"></asp:Label></button>
                    </div>
                    <asp:Panel ID="panelAddItem" runat="server" ValidationGroup="first">
                        <div class="modal fade" id="addItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lAddItem %>" CssClass="modal-title h5"></asp:Label>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div>
                                            <div class="mb-3">
                                                <label for="validationTextarea">
                                                    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lName %>"></asp:Label>
                                                </label>
                                                <asp:TextBox CssClass="form-control" ID="tbItemName" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ValidationGroup="first" ID="validatorUsername" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbItemName" CssClass="invalid-feedback2">
                                                    <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, lInvalidName %>"></asp:Label>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                            <div class="mb-3">
                                                <label for="validationTextarea3">
                                                    <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lPrice %>"></asp:Label>
                                                </label>
                                                <asp:TextBox CssClass="form-control" ID="tbPrice" runat="server" TextMode="Number"></asp:TextBox>
                                                <asp:RequiredFieldValidator ValidationGroup="first" ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbPrice" CssClass="invalid-feedback2">
                                                    <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, lInvalidPrice %>"></asp:Label>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                            <div class="mb-3">
                                                <label for="validationTextarea3">
                                                    <asp:Label ID="Label33" runat="server" Text="<%$ Resources:Resource, lPriceNew %>"></asp:Label>
                                                </label>
                                                <asp:TextBox CssClass="form-control" ID="tbPriceNew" runat="server" TextMode="Number" Text="0"></asp:TextBox>
                                            </div>
                                            <div class="mb-3">
                                                <label for="validationTextarea4">
                                                    <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lInStock %>"></asp:Label>
                                                </label>
                                                <asp:TextBox CssClass="form-control" ID="tbInStock" runat="server" TextMode="Number"></asp:TextBox>
                                                <asp:RequiredFieldValidator ValidationGroup="first" ID="RequiredFieldValidator2" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbInStock" CssClass="invalid-feedback2">
                                                    <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, lInvalidInStock %>"></asp:Label>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                            <div class="mb-3">
                                                <label for="validationTextarea4">
                                                    <asp:Label ID="Label35" runat="server" Text="<%$ Resources:Resource, color %>"></asp:Label>
                                                </label>
                                                <asp:TextBox CssClass="form-control" ID="tbColor" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ValidationGroup="first" ID="RequiredFieldValidator11" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbColor" CssClass="invalid-feedback2">
                                                    <asp:Label ID="Label36" runat="server" Text="<%$ Resources:Resource, lInvalidInStock %>"></asp:Label>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                            <div class="col-auto my-1 form-row align-items-center">
                                                <asp:CheckBoxList ID="cbListSize" runat="server" CssClass="custom-control custom-checkbox mr-sm-2" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="XS"></asp:ListItem>
                                                    <asp:ListItem Value="S"></asp:ListItem>
                                                    <asp:ListItem Value="M"></asp:ListItem>
                                                    <asp:ListItem Value="L"></asp:ListItem>
                                                    <asp:ListItem Value="XL"></asp:ListItem>
                                                </asp:CheckBoxList>

                                            </div>
                                            <div class="custom-control custom-radio">
                                                <asp:RadioButtonList ID="rbListSex" runat="server">
                                                    <asp:ListItem Text="<%$ Resources:Resource, lFemale %>" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:Resource, lMale %>" Value="M" Selected="True"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                            <div class="form-group">
                                                <asp:DropDownList CssClass="custom-select" ID="ddlCategories" runat="server" DataSourceID="SqlDataSource3" DataTextField="names" DataValueField="name">
                                                </asp:DropDownList>
                                            </div>

                                            <div class="custom-file">
                                                <asp:FileUpload CssClass="custom-file-input" ID="fileUploadAddItems" runat="server" AllowMultiple="true" />
                                                <asp:Label CssClass="custom-file-label" for="validatedCustomFile" ID="lFiles" runat="server" Text="<%$ Resources:Resource, lChooseFile %>"></asp:Label>
                                                <asp:RequiredFieldValidator ValidationGroup="first" ID="RequiredFieldValidator3" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="fileUploadAddItems" CssClass="invalid-feedback2">
                                                    <asp:Label ID="Label10" runat="server" Text="<%$ Resources:Resource, lInvalidFile %>"></asp:Label>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            <asp:Label ID="Label32" runat="server" Text="<%$ Resources:Resource, lClose %>"></asp:Label></button>
                                        <asp:LinkButton ValidationGroup="first" ID="btnAddItem" runat="server" CssClass="btn btn-primary" Text="<%$ Resources:Resource, lAddItem %>" OnClick="btnAddItem_Click"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panelUpdateItem" runat="server" ValidationGroup="updateItem">
                        <div class="modal fade" id="updateItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <asp:Label ID="Label39" runat="server" Text="<%$ Resources:Resource, lUpdateItem %>" CssClass="modal-title h5"></asp:Label>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div>
                                            <div class="mb-3">
                                                <label for="validationTextarea">
                                                    <asp:Label ID="Label40" runat="server" Text="<%$ Resources:Resource, lName %>"></asp:Label>
                                                </label>
                                                <asp:TextBox CssClass="form-control" ID="tbUpdateItemName" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ValidationGroup="updateItem" ID="RequiredFieldValidator6" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbUpdateItemName" CssClass="invalid-feedback2">
                                                    <asp:Label ID="Label41" runat="server" Text="<%$ Resources:Resource, lInvalidName %>"></asp:Label>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                            <div class="mb-3">
                                                <label for="validationTextarea3">
                                                    <asp:Label ID="Label42" runat="server" Text="<%$ Resources:Resource, lPrice %>"></asp:Label>
                                                </label>
                                                <asp:TextBox CssClass="form-control" ID="tbUpdatePrice" runat="server" TextMode="Number"></asp:TextBox>
                                                <asp:RequiredFieldValidator ValidationGroup="updateItem" ID="RequiredFieldValidator7" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbUpdatePrice" CssClass="invalid-feedback2">
                                                    <asp:Label ID="Label43" runat="server" Text="<%$ Resources:Resource, lInvalidPrice %>"></asp:Label>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                            <div class="mb-3">
                                                <label for="validationTextarea3">
                                                    <asp:Label ID="Label44" runat="server" Text="<%$ Resources:Resource, lPriceNew %>"></asp:Label>
                                                </label>
                                                <asp:TextBox CssClass="form-control" ID="tbUpdatePriceNew" runat="server" TextMode="Number" Text="0"></asp:TextBox>
                                            </div>
                                            <div class="mb-3">
                                                <label for="validationTextarea4">
                                                    <asp:Label ID="Label45" runat="server" Text="<%$ Resources:Resource, lInStock %>"></asp:Label>
                                                </label>
                                                <asp:TextBox CssClass="form-control" ID="tbUpdateInStock" runat="server" TextMode="Number"></asp:TextBox>
                                                <asp:RequiredFieldValidator ValidationGroup="updateItem" ID="RequiredFieldValidator10" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbUpdateInStock" CssClass="invalid-feedback2">
                                                    <asp:Label ID="Label46" runat="server" Text="<%$ Resources:Resource, lInvalidInStock %>"></asp:Label>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                            <div class="mb-3">
                                                <label for="validationTextarea4">
                                                    <asp:Label ID="Label47" runat="server" Text="<%$ Resources:Resource, color %>"></asp:Label>
                                                </label>
                                                <asp:TextBox CssClass="form-control" ID="tbUpdateColor" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ValidationGroup="updateItem" ID="RequiredFieldValidator12" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbUpdateColor" CssClass="invalid-feedback2">
                                                    <asp:Label ID="Label48" runat="server" Text="<%$ Resources:Resource, lInvalidInStock %>"></asp:Label>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                            <div class="col-auto my-1 form-row align-items-center">
                                                <asp:CheckBoxList ID="cblUpdateItem" runat="server" CssClass="custom-control custom-checkbox mr-sm-2" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="XS"></asp:ListItem>
                                                    <asp:ListItem Value="S"></asp:ListItem>
                                                    <asp:ListItem Value="M"></asp:ListItem>
                                                    <asp:ListItem Value="L"></asp:ListItem>
                                                    <asp:ListItem Value="XL"></asp:ListItem>
                                                </asp:CheckBoxList>

                                            </div>
                                            <div class="custom-control custom-radio">
                                                <asp:RadioButtonList ID="rblUpdateItem" runat="server">
                                                    <asp:ListItem Text="<%$ Resources:Resource, lFemale %>" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:Resource, lMale %>" Value="M" Selected="True"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                            <div class="form-group">
                                                <asp:DropDownList CssClass="custom-select" ID="ddlUpdateItem" runat="server" DataSourceID="SqlDataSource3" DataTextField="names" DataValueField="name">
                                                </asp:DropDownList>
                                            </div>

                                            <div class="custom-file">
                                                <asp:FileUpload CssClass="custom-file-input" ID="fileUpdateUploadAddItems" runat="server" AllowMultiple="true" />
                                                <asp:Label CssClass="custom-file-label" for="validatedCustomFile" ID="Label49" runat="server" Text="<%$ Resources:Resource, lChooseFile %>"></asp:Label>
                                                <asp:RequiredFieldValidator ValidationGroup="updateItem" ID="RequiredFieldValidator13" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="fileUpdateUploadAddItems" CssClass="invalid-feedback2">
                                                    <asp:Label ID="Label50" runat="server" Text="<%$ Resources:Resource, lInvalidFile %>"></asp:Label>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            <asp:Label ID="Label51" runat="server" Text="<%$ Resources:Resource, lClose %>"></asp:Label></button>
                                        <asp:LinkButton ValidationGroup="updateItem" ID="lbtnUpdateItem" runat="server" CssClass="btn btn-primary" Text="<%$ Resources:Resource, lUpdateItem %>" OnClick="lbtnUpdateItem_Click"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panelDeleteItem" runat="server">
                        <!-- Modal -->
                        <div class="modal fade" id="deleteItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <asp:Label CssClass="modal-title" ID="Label15" runat="server" Text="<%$ Resources:Resource,lDeleteItem %>"></asp:Label>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <asp:Label ID="Label11" runat="server" Text="<%$ Resources:Resource, lDeleteItemText %>"></asp:Label>
                                        <asp:Label ID="Label12" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            <asp:Label ID="Label13" runat="server" Text="<%$ Resources:Resource, lClose %>"></asp:Label></button>
                                        <asp:LinkButton CssClass="btn btn-primary" ID="lbtnDeleteItem" runat="server" OnClick="lbtnDeleteItem_Click">
                                            <asp:Label ID="Label14" runat="server" Text="<%$ Resources:Resource, lDeleteItem %>"></asp:Label>
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>

                <p class="display-4 mt-3">
                    <asp:Label ID="Label53" runat="server" Text="<%$ Resources:Resource, lUsers %>"></asp:Label>
                </p>
                <div class="row">
                    <div class="col-md-8">
                        <asp:ListBox CssClass="custom-select m-1" ID="listBoxUsers" runat="server" DataSourceID="SqlDataSoutceUsers" DataTextField="loginType" DataValueField="id"></asp:ListBox>
                        <asp:LinkButton CssClass="btn btn-primary m-1" ID="LinkButton3" runat="server" OnClick="lbtnMakeAdmin_Click"><asp:Label ID="Label54" runat="server" Text="<%$ Resources:Resource, lMakeAdmin %>"></asp:Label></asp:LinkButton>
                        <asp:LinkButton CssClass="btn btn-primary m-1" ID="LinkButton4" runat="server" OnClick="lbtnMakeUser_Click"><asp:Label ID="Label62" runat="server" Text="<%$ Resources:Resource, lMakeUser %>"></asp:Label></asp:LinkButton>
                        <button type="button" class="btn btn-danger m-1" data-toggle="modal" data-target="#deleteUser">
                            <asp:Label ID="Label55" runat="server" Text="<%$ Resources:Resource, lDeleteUser %>"></asp:Label></button>
                    </div>
                    <div class="col-md">
                        <button type="button" class="btn btn-primary m-1" data-toggle="modal" data-target="#addUser">
                            <asp:Label ID="Label56" runat="server" Text="<%$ Resources:Resource, lAddUser %>"></asp:Label></button>
                    </div>
                    <asp:Panel ID="panelDeleteUser" runat="server">
                        <!-- Modal -->
                        <div class="modal fade" id="deleteUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <asp:Label CssClass="modal-title" ID="Label57" runat="server" Text="<%$ Resources:Resource,lDeleteItem %>"></asp:Label>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <asp:Label ID="Label58" runat="server" Text="<%$ Resources:Resource, lDeleteUserText %>"></asp:Label>
                                        <asp:Label ID="Label59" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            <asp:Label ID="Label60" runat="server" Text="<%$ Resources:Resource, lClose %>"></asp:Label></button>
                                        <asp:LinkButton CssClass="btn btn-primary" ID="LinkButton1" runat="server" OnClick="lbtnDeleteUser_Click">
                                            <asp:Label ID="Label61" runat="server" Text="<%$ Resources:Resource, lDeleteUser %>"></asp:Label>
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                     <asp:Panel ID="panelAddUser" runat="server">
                        <div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <asp:Label ID="Label63" runat="server" Text="<%$ Resources:Resource, lAddUser %>" CssClass="modal-title h5"></asp:Label>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div>
                                            <div class="mb-3">
                                                <label for="validationTextarea">
                                                    <asp:Label ID="Label64" runat="server" Text="<%$ Resources:Resource, lName %>"></asp:Label>
                                                </label>
                                                <asp:TextBox CssClass="form-control" ID="tbLogin" runat="server" ValidationGroup="addUser"></asp:TextBox>
                                                <asp:RequiredFieldValidator ValidationGroup="addUser" ID="RequiredFieldValidator14" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbLogin" CssClass="invalid-feedback2">
                                                    <asp:Label ID="Label65" runat="server" Text="<%$ Resources:Resource, lInvalidName %>"></asp:Label>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                            <div class="mb-3">
                                                <label for="validationTextarea">
                                                    <asp:Label ID="Label66" runat="server" Text="<%$ Resources:Resource, lEmail %>"></asp:Label>
                                                </label>
                                                <asp:TextBox CssClass="form-control" ID="tbEmaill" TextMode="Email" runat="server" ValidationGroup="addUser"></asp:TextBox>
                                                <asp:RequiredFieldValidator ValidationGroup="addUser" ID="RequiredFieldValidator15" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbEmaill" CssClass="invalid-feedback2">
                                                    <asp:Label ID="Label67" runat="server" Text="<%$ Resources:Resource, lInvalidEmail %>"></asp:Label>
                                                </asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" ID="RegularExpressionValidator1" ControlToValidate="tbEmaill" runat="server" CssClass="invalid-feedback2" ErrorMessage="RegularExpressionValidator">
                                                    <asp:Label ID="Label68" runat="server" Text="<%$ Resources:Resource, lInvalidEmailConstruction %>"></asp:Label>

                                                </asp:RegularExpressionValidator>
                                            </div>
                                            <div class="mb-3">
                                                <label for="validationTextarea">
                                                    <asp:Label ID="Label69" runat="server" Text="<%$ Resources:Resource, lPassword %>"></asp:Label>
                                                </label>
                                                <asp:TextBox CssClass="form-control" ID="tbPassword5" TextMode="Password" runat="server" ValidationGroup="addUser"></asp:TextBox>
                                                <asp:RequiredFieldValidator ValidationGroup="addUser" ID="RequiredFieldValidator16" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbPassword5" CssClass="invalid-feedback2">
                                                    <asp:Label ID="Label70" runat="server" Text="<%$ Resources:Resource, lInvalidPassword %>"></asp:Label>
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <asp:LinkButton ValidationGroup="addUser" ID="LinkButton2" runat="server" CssClass="btn btn-primary" Text="<%$ Resources:Resource, lAddUser %>" OnClick="btnAddUser_Click"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>

                <p class="display-4 mt-3">
                    <asp:Label ID="Label75" runat="server" Text="<%$ Resources:Resource, lOrders %>"></asp:Label>
                </p>

                <div class="row">
                    <div class="col-md-8">
                        <asp:ListBox CssClass="custom-select m-1" ID="lbOrders" runat="server" DataSourceID="SqlDataSourceOrders" DataTextField="name" DataValueField="id"></asp:ListBox>
                        <asp:LinkButton CssClass="btn btn-primary m-1" ID="LinkButton5" runat="server" OnClick="lbtnPlaced_Click"><asp:Label ID="Label71" runat="server" Text="<%$ Resources:Resource, lMakePlaced %>"></asp:Label></asp:LinkButton>
                        <asp:LinkButton CssClass="btn btn-primary m-1" ID="LinkButton6" runat="server" OnClick="lbtnInProgress_Click"><asp:Label ID="Label72" runat="server" Text="<%$ Resources:Resource, lMakeInProgress %>"></asp:Label></asp:LinkButton>
                        <asp:LinkButton CssClass="btn btn-primary m-1" ID="LinkButton7" runat="server" OnClick="lbtnShipped_Click"><asp:Label ID="Label76" runat="server" Text="<%$ Resources:Resource, lMakeShipped %>"></asp:Label></asp:LinkButton>
                        <asp:LinkButton CssClass="btn btn-danger m-1" ID="LinkButton8" runat="server" OnClick="lbtnCancelled_Click"><asp:Label ID="Label77" runat="server" Text="<%$ Resources:Resource, lMakeCancelled %>"></asp:Label></asp:LinkButton>
                    </div>
                    <div class="col-md">
                    </div>
                    
                     
                </div>
            </div>
            <footer class="container">
                <p class="text-center">by Rafał Matuszyk</p>
            </footer>
        </div>
    </form>
</body>
</html>
