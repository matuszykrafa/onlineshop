﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="OnlineShop.Contact" Culture="auto:en-US" UICulture="auto" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="assets/favicon.ico">
    <title>Clothesland</title>
    <script src="js/jquery-3.4.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <form id="form1" runat="server">
        <div class="container-fluid website bg-light">
            <header class="container">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
                        aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <img class="logo" src="assets/logo2.png" alt="" />Clothesland</a>

                    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li class="nav-item dropdown">
                                <asp:Panel runat="server" ID="PanelDropdownMale">
                                </asp:Panel>
                            </li>
                            <li class="nav-item dropdown">
                                <asp:Panel runat="server" ID="PanelDropdownFemale">
                                </asp:Panel>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/Contact.aspx">
                                    <asp:Label ID="Label3" runat="server" Text="<%$Resources: Resource, lContact %>"></asp:Label></a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <div class="dropdown-divider"></div>
                            </li>
                            <li>
                                <a class="nav-link" href="/Cart.aspx">
                                    <asp:Label ID="Label9" runat="server" Text="<%$Resources: Resource, lCart %>"></asp:Label></a>
                            </li>
                            <asp:Label ID="lLoginButtons" runat="server" CssClass="nav navbar-nav navbar-right"></asp:Label>
                            <li>
                                <div class="dropdown-divider"></div>
                            </li>
                            <li>
                                <asp:LinkButton ID="lbtnLanguage" runat="server" CssClass="nav-link" Text="<%$ Resources:Resource, lLang %>" OnClick="lbtnLanguage_Click" CausesValidation="False"></asp:LinkButton></li>
                        </ul>
                    </div>
                </nav>
            </header>
            <div id="content" class="container">

                <div class="container contact-form">
                    <div class="form-group row">
                        <asp:Label ID="Label5" runat="server" CssClass="display-4" Text="<%$ Resources:Resource, lContact %>"></asp:Label>
                    </div>
                    <div class="form-group row">
                        <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, lEmail %>" CssClass="col-sm-2 col-form-label"></asp:Label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="tbEmail" runat="server" CssClass="form-control" TextMode="Email"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="validateEmail" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbEmail" CssClass="invalid-feedback2">
                                <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lInvalidEmail %>"></asp:Label>
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" ID="RegularExpressionValidator1" ControlToValidate="tbEmail" runat="server" CssClass="invalid-feedback2" ErrorMessage="RegularExpressionValidator">
                                                    <asp:Label ID="Label68" runat="server" Text="<%$ Resources:Resource, lInvalidEmailConstruction %>"></asp:Label>

                                                </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="form-group row">
                        <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lSubject %>" CssClass="col-sm-2 col-form-label"></asp:Label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="tbSubject" runat="server" CssClass="form-control" TextMode="SingleLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbSubject" CssClass="invalid-feedback2">
                                <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, lInvalidSubject %>"></asp:Label>
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group row">
                        <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, lMessage %>" CssClass="col-sm-2 col-form-label"></asp:Label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="tbMessage" runat="server" CssClass="form-control" Rows="10" TextMode="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbMessage" CssClass="invalid-feedback2">
                                <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lInvalidMessage %>"></asp:Label>
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-10">
                            <asp:Button OnClick="btnSendMessage_Click" ID="btnSendMessage" runat="server" Text="<%$ Resources:Resource, lSend %>" CssClass="btn btn-primary" />
                        </div>
                    </div>
                </div>

            </div>
            <footer class="container">
                <p class="text-center">by Rafał Matuszyk</p>
            </footer>
        </div>
    </form>
</body>

</html>
