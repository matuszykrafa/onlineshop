﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="OnlineShop.Default" Culture="auto:en-US" UICulture="auto" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="assets/favicon.ico">
    <title>Clothesland</title>
    <script src="js/jquery-3.4.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <form id="form1" runat="server">
        <div class="container-fluid website bg-light">
            <header class="container">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
                        aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <img class="logo" src="assets/logo2.png" alt="" />Clothesland</a>

                    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li class="nav-item dropdown">
                                <asp:Panel runat="server" ID="PanelDropdownMale">
                                </asp:Panel>
                            </li>
                            <li class="nav-item dropdown">
                                <asp:Panel runat="server" ID="PanelDropdownFemale">
                                </asp:Panel>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/Contact.aspx">
                                    <asp:Label ID="Label2" runat="server" Text="<%$Resources: Resource, lContact %>"></asp:Label></a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <div class="dropdown-divider"></div>
                            </li>
                            <li>
                                <a class="nav-link" href="/Cart.aspx">
                                    <asp:Label ID="Label3" runat="server" Text="<%$Resources: Resource, lCart %>"></asp:Label></a>
                            </li>
                            <asp:Label ID="lLoginButtons" runat="server" CssClass="nav navbar-nav navbar-right"></asp:Label>
                            <li>
                                <div class="dropdown-divider"></div>
                            </li>
                            <li>
                                <asp:LinkButton ID="lbtnLanguage" runat="server" CssClass="nav-link" Text="<%$ Resources:Resource, lLang %>" OnClick="lbtnLanguage_Click" CausesValidation="False"></asp:LinkButton></li>
                        </ul>
                    </div>
                </nav>
            </header>
            <div id="content" class="container">

                <div class="container specials">
                    <div class="container specials-text">
                        <h1 class="display-3 text-center">
                            <asp:Label ID="lSpecialOffer" runat="server" Text="<%$ Resources:Resource, lSpecialOffer %>"></asp:Label></h1>
                    </div>
                    <div class="row">

                        <div class="col-sm">
                            <div class="item">
                                <a href="offer.aspx?id=2" class="item-a">
                                    <div class="item-image">
                                        <img src="/assets/2/1.png" alt="Card Back">
                                        <img src="/assets/2/2.png" class="img-top" alt="Card Front">
                                    </div>
                                    <p class="h5">
                                        Cena <span class="text-danger">$13.59</span><span
                                            class="text-muted old_price">$17.99</span>
                                    </p>
                                    <p class="h6">Spodnie jakieśtam super beka chce dwie linie plis</p>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="item">
                                <a href="offer.aspx?id=1" class="item-a">
                                    <div class="item-image">
                                        <img src="/assets/1/1.png" alt="Card Back">
                                        <img src="/assets/1/2.png" class="img-top" alt="Card Front">
                                    </div>
                                    <p class="h5">
                                        Cena <span class="text-danger">$13.59</span><span
                                            class="text-muted old_price">$17.99</span>
                                    </p>
                                    <p class="h6">Spodnie jakieśtam super beka chce dwie linie plis</p>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="item">
                                <a href="offer.aspx" class="item-a">
                                    <div class="item-image">
                                        <img src="/assets/1.png" alt="Card Back">
                                        <img src="/assets/2.png" class="img-top" alt="Card Front">
                                    </div>
                                    <p class="h5">
                                        Cena <span class="text-danger">$13.59</span><span
                                            class="text-muted old_price">$17.99</span>
                                    </p>
                                    <p class="h6">Spodnie jakieśtam super beka chce dwie linie plis</p>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="item">
                                <a href="offer.aspx" class="item-a">
                                    <div class="item-image">
                                        <img src="/assets/1.png" alt="Card Back">
                                        <img src="/assets/2.png" class="img-top" alt="Card Front">
                                    </div>
                                    <p class="h5">
                                        Cena <span class="text-danger">$13.59</span><span
                                            class="text-muted old_price">$17.99</span>
                                    </p>
                                    <p class="h6">Spodnie jakieśtam super beka chce dwie linie plis</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container specials">
                    <div class="container specials-text">
                        <h1 class="display-4 text-center">
                            <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lTrending %>"></asp:Label></h1>
                    </div>
                    <div class="row">

                        <div class="col-sm">
                            <div class="item">
                                <a href="offer.aspx" class="item-a">
                                    <div class="item-image">
                                        <img src="/assets/1.png" alt="Card Back">
                                        <img src="/assets/2.png" class="img-top" alt="Card Front">
                                    </div>
                                    <p class="h5">
                                        Cena <span class="text-danger">$13.59</span><span
                                            class="text-muted old_price">$17.99</span>
                                    </p>
                                    <p class="h6">Spodnie jakieśtam super beka chce dwie linie plis</p>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="item">
                                <a href="offer.aspx" class="item-a">
                                    <div class="item-image">
                                        <img src="/assets/spodnie.png" alt="Card Back">
                                        <img src="/assets/spodnie2.png" class="img-top" alt="Card Front">
                                    </div>
                                    <p class="h5">
                                        Cena <span class="text-danger">$13.59</span><span
                                            class="text-muted old_price">$17.99</span>
                                    </p>
                                    <p class="h6">Spodnie jakieśtam super beka chce dwie linie plis</p>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="item">
                                <a href="offer.aspx" class="item-a">
                                    <div class="item-image">
                                        <img src="/assets/1.png" alt="Card Back">
                                        <img src="/assets/2.png" class="img-top" alt="Card Front">
                                    </div>
                                    <p class="h5">
                                        Cena <span class="text-danger">$13.59</span><span
                                            class="text-muted old_price">$17.99</span>
                                    </p>
                                    <p class="h6">Spodnie jakieśtam super beka chce dwie linie plis</p>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="item">
                                <a href="offer.aspx" class="item-a">
                                    <div class="item-image">
                                        <img src="/assets/1.png" alt="Card Back">
                                        <img src="/assets/2.png" class="img-top" alt="Card Front">
                                    </div>
                                    <p class="h5">
                                        Cena <span class="text-danger">$13.59</span><span
                                            class="text-muted old_price">$17.99</span>
                                    </p>
                                    <p class="h6">Spodnie jakieśtam super beka chce dwie linie plis</p>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="item">
                                <a href="offer.aspx" class="item-a">
                                    <div class="item-image">
                                        <img src="/assets/spodnie.png" alt="Card Back">
                                        <img src="/assets/spodnie2.png" class="img-top" alt="Card Front">
                                    </div>
                                    <p class="h5">
                                        Cena <span class="text-danger">$13.59</span><span
                                            class="text-muted old_price">$17.99</span>
                                    </p>
                                    <p class="h6">Spodnie jakieśtam super beka chce dwie linie plis</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="container">
                <p class="text-center">by Rafał Matuszyk</p>
            </footer>
        </div>
    </form>
</body>

</html>
