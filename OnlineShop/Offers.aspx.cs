﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineShop
{
    public partial class Offers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sex = HttpUtility.ParseQueryString(Request.Url.Query).Get("sex");
            string category = HttpUtility.ParseQueryString(Request.Url.Query).Get("category");
            if (sex != null && category != null)
            {
                if (Session["user"] != null)
                {
                    Generator.GenerateHelloLabel(lLoginButtons, Session["user"].ToString());
                }
                else
                {
                    Generator.GenerateLoginButtons(lLoginButtons);
                }
                Generator.GenerateDropdownCategory(PanelDropdownMale, Resources.Resource.lMale);
                Generator.GenerateDropdownCategory(PanelDropdownFemale, Resources.Resource.lFemale);
                Generator.SetLabelTextCategoryAndSex(lCategoryAndSex, sex, category);
                Generator.GenerateOffers(panelOffers, 3, sex, category);
            }
            else
            {
                Response.Redirect("/");
            }
            
        }

        protected void lbtnLanguage_Click(object sender, EventArgs e)
        {
            string option = lbtnLanguage.Text.ToString();
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(option);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(option);
            Session["culture"] = option;
            Response.Redirect(Request.Url.PathAndQuery);
        }

        protected override void InitializeCulture()
        {
            if (Session["culture"] != null)
            {
                UICulture = Session["culture"].ToString();
                Culture = Session["culture"].ToString();
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Session["culture"].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["culture"].ToString());
            }
            base.InitializeCulture();
        }
    }
}