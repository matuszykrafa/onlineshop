﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Offer.aspx.cs" Inherits="OnlineShop.Offer" Culture="auto:en-US" UICulture="auto" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Clothesland</title>
    <link rel="icon" type="image/png" href="assets/favicon.ico">
    <script src="js/jquery-3.4.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="css/offer.css">
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid website bg-light">

            <header class="container">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
                        aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <img class="logo" src="assets/logo2.png" alt="" />Clothesland</a>

                    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li class="nav-item dropdown">
                                <asp:Panel runat="server" ID="PanelDropdownMale">
                                </asp:Panel>
                            </li>
                            <li class="nav-item dropdown">
                                <asp:Panel runat="server" ID="PanelDropdownFemale">
                                </asp:Panel>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/Contact.aspx">
                                    <asp:Label ID="Label2" runat="server" Text="<%$Resources: Resource, lContact %>"></asp:Label></a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                             <li>
                                <div class="dropdown-divider"></div>
                            </li>
                            <li>
                                <a class="nav-link" href="/Cart.aspx">
                                    <asp:Label ID="Label3" runat="server" Text="<%$Resources: Resource, lCart %>"></asp:Label></a>
                            </li>
                            <asp:Label ID="lLoginButtons" runat="server" CssClass="nav navbar-nav navbar-right"></asp:Label>
                            <li>
                                <asp:LinkButton ID="lbtnLanguage" runat="server" CssClass="nav-link" Text="<%$ Resources:Resource, lLang %>" OnClick="lbtnLanguage_Click" CausesValidation="False"></asp:LinkButton></li>
                        </ul>
                    </div>
                </nav>
            </header>
            <div id="content" class="container">
                <div class="row">
                    <div class="col-lg">
                        <div class="container offer">
                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                <asp:Panel ID="panelImagesCount" runat="server">
                                </asp:Panel>
                                <div class="carousel-inner">
                                    <asp:Panel ID="panelImages" runat="server">
                                    </asp:Panel>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="container offer-info">
                            <div class="offer-name">
                                <asp:Panel ID="panelOfferName" runat="server">
                                </asp:Panel>
                            </div>
                            <div class="offer-shop">
                                <div class="size">
                                    <asp:Panel ID="panelOfferSize" runat="server">
                                    </asp:Panel>
                                </div>
                                <div class="add-to-cart">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <asp:LinkButton ValidationGroup="quantity" OnClick="lbtnAddToCart_Click" ID="lbtnAddToCart" CssClass="btn btn-primary btn-md btn-block mt-3" runat="server">
                                                <asp:Label ID="Label1" runat="server" Text="<% $ Resources: Resource, lAddToCart %>"></asp:Label></asp:LinkButton>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbQuantity" ValidationGroup="quantity" runat="server" CssClass="form-control mt-3" TextMode="Number" Text="1"></asp:TextBox>
                                            <asp:RangeValidator CssClass="invalid-feedback2" ControlToValidate="tbQuantity" ID="RangeValidator1" runat="server" ErrorMessage="<% $Resources: Resource, lQuantityValidator %>" MaximumValue="99" MinimumValue="1" Type="Integer" ValidationGroup="quantity"></asp:RangeValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="container">
                <p class="text-center">by Rafał Matuszyk</p>
            </footer>
        </div>
    </form>
</body>
</html>
