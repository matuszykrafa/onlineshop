﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineShop
{
    public partial class Settings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                Generator.GenerateHelloLabel(lLoginButtons, Session["user"].ToString());
            }
            else
            {
                Response.Redirect("/Login.aspx");
            }
            Generator.GenerateDropdownCategory(PanelDropdownMale, Resources.Resource.lMale);
            Generator.GenerateDropdownCategory(PanelDropdownFemale, Resources.Resource.lFemale);
        }

        protected void lbtnLanguage_Click(object sender, EventArgs e)
        {
            string option = lbtnLanguage.Text.ToString();
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(option);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(option);
            Session["culture"] = option;
            Response.Redirect(Request.Url.PathAndQuery);
        }

        protected override void InitializeCulture()
        {
            if (Session["culture"] != null)
            {
                UICulture = Session["culture"].ToString();
                Culture = Session["culture"].ToString();
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Session["culture"].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["culture"].ToString());
            }
            base.InitializeCulture();
        }

        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
            int changePassword = OnlineShop.User.ChangePassword(Session["user"].ToString(), tbOldPassword.Text.ToString(), tbPassword.Text.ToString());
            string message = "";
            if (changePassword == 0)
            {
                message = Resources.Resource.changePasswordProblem.ToString();
            }
            else if (changePassword == -1)
            {
                message = Resources.Resource.databaseProblem.ToString();
            }
            else if(changePassword == 1)
            {
                message = Resources.Resource.changePasswordSuccess.ToString();
            }
            string alert = "<script language =\"javascript\" type=\"text/javascript\">alert('" + message + "');</script>";
            Response.Write(alert);
        }
    }
}