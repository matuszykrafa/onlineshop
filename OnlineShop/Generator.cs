﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace OnlineShop
{
    public class Generator
    {
        public static void GenerateHelloLabel(Label label, string username)
        {
            if (System.Web.HttpContext.Current.Session["user_type"].ToString() == "admin")
            {
                HtmlGenericControl li0 = new HtmlGenericControl("li");
                HtmlGenericControl aSettingsAdmin = new HtmlGenericControl("a");
                aSettingsAdmin.Attributes["class"] = "nav-link text-danger";
                aSettingsAdmin.InnerText = Resources.Resource.lSettingsAdmin.ToString();
                aSettingsAdmin.Attributes["href"] = "/Admin.aspx";

                //lBtnAdmin.Click += new EventHandler(User.Logout);
                li0.Controls.Add(aSettingsAdmin);

                label.Controls.Add(li0);
            }

            HtmlGenericControl aSettings = new HtmlGenericControl("a");
            aSettings.InnerText = Resources.Resource.lHello.ToString() + ", " + username;
            aSettings.Attributes["class"] = "nav-link helloText";
            aSettings.Attributes["href"] = "/Settings.aspx";

            HtmlGenericControl li = new HtmlGenericControl("li");
            LinkButton lBtn = new LinkButton();
            lBtn.CssClass = "nav-link";
            lBtn.Text = Resources.Resource.lLogout.ToString();
            lBtn.CausesValidation = false;

            lBtn.Click += new EventHandler(User.Logout);
            li.Controls.Add(lBtn);

            label.Controls.Add(aSettings);
            label.Controls.Add(li);
        }

        

        internal static void GenerateCart(Panel panelCart, string user_id)
        {
            List<cartPosition> carts = Database.getCartPositions(user_id);
            if (carts.Count > 0)
            {
                Debug.WriteLine(carts[0].quantity);
                float suma = 0;
                foreach (cartPosition cart in carts)
                {

                    HtmlGenericControl div = new HtmlGenericControl("div");
                    div.Attributes["class"] = "row border-top p-3";

                    HtmlGenericControl div2 = new HtmlGenericControl("div");
                    div2.Attributes["class"] = "col-6";
                    div.Controls.Add(div2);

                    HtmlGenericControl p1 = new HtmlGenericControl("p");
                    p1.Attributes["class"] = "h5";
                    HtmlGenericControl a = new HtmlGenericControl("a");
                    a.InnerText = cart.name;
                    a.Attributes["href"] = "offer.aspx?id=" + cart.id_item;
                    p1.Controls.Add(a);
                    div2.Controls.Add(p1);

                    HtmlGenericControl div21 = new HtmlGenericControl("div");
                    div21.Attributes["class"] = "col-1";
                    div.Controls.Add(div21);

                    HtmlGenericControl p21 = new HtmlGenericControl("p");
                    p21.Attributes["class"] = "h5";
                    p21.InnerText = cart.size;
                    div21.Controls.Add(p21);

                    HtmlGenericControl div3 = new HtmlGenericControl("div");
                    div3.Attributes["class"] = "col-sm";
                    div.Controls.Add(div3);

                    TextBox tb = new TextBox
                    {
                        CssClass = "form-control",
                        TextMode = TextBoxMode.Number,
                        AutoPostBack = true,
                        Text = cart.quantity
                    };
                    tb.TextChanged += (sender, e) => Cart.TbQuantity_Click(sender, e, cart.id);
                    div3.Controls.Add(tb);


                    HtmlGenericControl div4 = new HtmlGenericControl("div");
                    div4.Attributes["class"] = "col-sm-2";
                    div.Controls.Add(div4);

                    HtmlGenericControl p2 = new HtmlGenericControl("p");
                    p2.Attributes["class"] = "h5";
                    p2.InnerText = "$" + cart.price;
                    suma += float.Parse(cart.price) * float.Parse(cart.quantity);
                    div4.Controls.Add(p2);

                    HtmlGenericControl div8 = new HtmlGenericControl("div");
                    div8.Attributes["class"] = "col-sm-1";
                    div.Controls.Add(div8);

                    LinkButton bt = new LinkButton();
                    bt.Click += (sender, e) => Cart.btDeletePosition_Click(sender, e, cart.id);
                    bt.CssClass = "btn btn-danger";
                    bt.Text = "&#10005;";
                    div8.Controls.Add(bt);

                    panelCart.Controls.Add(div);
                }
                HtmlGenericControl div5 = new HtmlGenericControl("div");
                div5.Attributes["class"] = "row";
                panelCart.Controls.Add(div5);

                HtmlGenericControl div6 = new HtmlGenericControl("div");
                div6.Attributes["class"] = "col text-right";
                div5.Controls.Add(div6);

                HtmlGenericControl p6 = new HtmlGenericControl("p");
                p6.Attributes["class"] = "h4";
                p6.InnerText = Resources.Resource.lTotal + ": $" + suma.ToString();
                div6.Controls.Add(p6);

                LinkButton btOrder = new LinkButton();
                btOrder.Click += (sender, e) => Cart.btOrder_Click(sender, e, carts, user_id);
                btOrder.CssClass = "btn btn-primary";
                btOrder.Text = Resources.Resource.lOrder;
                div6.Controls.Add(btOrder);
            }
            else
            {
                HtmlGenericControl p = new HtmlGenericControl("p");
                p.Attributes["class"] = "h4";
                p.InnerText = Resources.Resource.emptyCart;
                panelCart.Controls.Add(p);
            }
        }

        public static void SetLabelTextCategoryAndSex(Label label, string sex, string category)
        {
            if (sex == "M")
            {
                label.Text += Resources.Resource.lMens;
            }
            else
            {
                label.Text += Resources.Resource.lFemales;
            }
            try
            {
                if (HttpContext.Current.Session["culture"] != null)
                {
                    if (HttpContext.Current.Session["culture"].ToString() == "PL")
                    {
                        label.Text += " " + Database.SelectCategory(category).namePL;
                    }
                    else
                    {
                        label.Text += " " + Database.SelectCategory(category).name;
                    }
                }
                else
                {
                    label.Text += " " + Database.SelectCategory(category).name;
                }
            }
            catch
            {
                Debug.WriteLine("Błąd przy pobieraniu kategori w generatorze");
            }
        }

        public static void GenerateDropdownCategory(Panel panel, string sex)
        {
            HtmlGenericControl a = new HtmlGenericControl("a");
            a.Attributes["class"] = "nav-link dropdown-toggle";
            a.Attributes["href"] = "#";
            a.ID = "navbarDropdownMenuLink" + sex;
            a.Attributes["role"] = "button";
            a.Attributes["data-toggle"] = "dropdown";
            a.Attributes["aria-haspopup"] = "true";
            a.Attributes["aria-expanded"] = "false";
            a.InnerText = sex;

            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = "dropdown-menu";
            div.Attributes["aria-labelledby"] = "navbarDropdownMenuLink";
            panel.Controls.Add(a);
            panel.Controls.Add(div);


            try
            {
                List<Category> categories = Database.SelectCategories();
                int count = categories.Count;
                for (int i = 0; i < count; i++)
                {
                    HtmlGenericControl aItem = new HtmlGenericControl("a");
                    aItem.Attributes["class"] = "dropdown-item";
                    char sexS = sex[0];
                    if (sexS == 'K') sexS = 'F';
                    aItem.Attributes["href"] = "/offers.aspx?sex=" + sexS + "&category=" + categories[i].name;
                    if (System.Web.HttpContext.Current.Session["culture"] != null)
                    {
                        if (System.Web.HttpContext.Current.Session["culture"].ToString() == "PL")
                        {
                            aItem.InnerText = categories[i].namePL;
                        }
                        else
                        {
                            aItem.InnerText = categories[i].name;
                        }
                    }
                    else
                    {
                        aItem.InnerText = categories[i].name;
                    }
                    div.Controls.Add(aItem);
                }
            }
            catch
            {
                Debug.WriteLine("Błąd przy pobieraniu danych o kategoriach w generatorze");
            }


        }

        //public static void GenerateCategoriesList(Panel panel)
        //{
        //    HtmlGenericControl option1 = new HtmlGenericControl("option");
        //    option1.Attributes["value"] = "";
        //    option1.InnerText = Resources.Resource.lSelectCategory.ToString();
        //    panel.Controls.Add(option1);
        //    try
        //    {
        //        List<string> categories = Database.SelectCategories();
        //        int count = categories.Count;
        //        for (int i = 0; i < count; i++)
        //        {
        //            HtmlGenericControl option = new HtmlGenericControl("option");
        //            option.Attributes["value"] = categories[i];
        //            option.InnerText = categories[i];
        //            panel.Controls.Add(option);
        //        }
        //    }
        //    catch
        //    {
        //        Debug.WriteLine("Błąd przy pobieraniu danych o kategoriach w generatorze");
        //    }
        //}

        public static void GenerateLoginButtons(Label label)
        {
            HtmlGenericControl li = new HtmlGenericControl("li");
            HtmlGenericControl a = new HtmlGenericControl("a");
            a.Attributes["href"] = "register.aspx";
            a.Attributes["class"] = "nav-link";

            Label lRegister = new Label();
            lRegister.ID = "lRegister";
            lRegister.Text = Resources.Resource.lRegister.ToString();
            a.Controls.Add(lRegister);
            li.Controls.Add(a);

            HtmlGenericControl li2 = new HtmlGenericControl("li");
            HtmlGenericControl a2 = new HtmlGenericControl("a");
            a2.Attributes["href"] = "login.aspx";
            a2.Attributes["class"] = "nav-link";

            Label lLogin = new Label();
            lLogin.ID = "lLogin";
            lLogin.Text = Resources.Resource.lLogin.ToString();
            a2.Controls.Add(lLogin);
            li2.Controls.Add(a2);

            label.Controls.Add(li);
            label.Controls.Add(li2);
        }

        public static dynamic GeneratePanelImagesCount(Panel panel, string id)
        {
            dynamic item = Database.SelectItem(id);
            try
            {
                HtmlGenericControl ol = new HtmlGenericControl("ol");
                ol.Attributes["class"] = "carousel-indicators";
                int imagesCount = item.images.Length;
                for (int i = 0; i < imagesCount; i++)
                {
                    HtmlGenericControl li = new HtmlGenericControl("li");
                    li.Attributes["data-targat"] = "#carouselExampleIndicators";
                    li.Attributes["data-slide-to"] = i.ToString();
                    if (i == 0)
                    {
                        li.Attributes["class"] = "active";
                    }
                    ol.Controls.Add(li);
                }

                panel.Controls.Add(ol);
                return item;
            }
            catch
            {
                Debug.WriteLine("Błąd przy tworzeniu panelu PanelImagesCount");
                return 0;
            }

        }

        public static void GeneratePanelImages(Panel panel, string id, Item item)
        {
            int imagesCount = item.images.Length;
            for (int i = 0; i < imagesCount; i++)
            {
                HtmlGenericControl div = new HtmlGenericControl("div");
                if (i == 0)
                {
                    div.Attributes["class"] = "carousel-item active";
                }
                else
                {
                    div.Attributes["class"] = "carousel-item";
                }
                HtmlGenericControl img = new HtmlGenericControl("img");
                img.Attributes["class"] = "d-block w-100";
                img.Attributes["alt"] = i.ToString() + " slide";
                img.Attributes["src"] = "/assets/" + id.ToString() + "/" + item.images[i];

                div.Controls.Add(img);
                panel.Controls.Add(div);
            }
        }

        public static void GeneratePanelOfferName(Panel panel, string id, Item item)
        {
            HtmlGenericControl p = new HtmlGenericControl("p");
            p.Attributes["class"] = "h3";
            p.InnerText = item.name;

            HtmlGenericControl p2 = new HtmlGenericControl("p");
            p2.InnerHtml = Resources.Resource.color + ": <b>" + item.color + "</b> " + Resources.Resource.quantity + ": <b>" + item.stock.ToString() + "</b>";

            HtmlGenericControl p3 = new HtmlGenericControl("p");
            p3.Attributes["class"] = "h5";
            if (item.price_new > 0)
            {
                p3.InnerHtml = "Cena: <span class=\"text-danger\">$" + item.price_new + "</span>" +
                    "<span class=\"text-muted old_price\">$" + item.price + "</span><span class=\"offer-vat\">w tym VAT</span>";
            }
            else
            {
                p3.InnerHtml = "Cena: <span>$" + item.price + "</span><span class=\"offer-vat\">w tym VAT</span>";
            }

            panel.Controls.Add(p);
            panel.Controls.Add(p2);
            panel.Controls.Add(p3);
        }

        public static void GeneratePanelOfferSize(Panel panel, string id, Item item)
        {
            HtmlGenericControl select = new HtmlGenericControl("select");
            select.Attributes["class"] = "custom-select custom-select-lg mb-3";
            select.Attributes["name"] = "selectSize";
            select.ID = "selectSize";

            //HtmlGenericControl option = new HtmlGenericControl("option");
            //option.Attributes["selected"] = "true";
            //option.InnerText = Resources.Resource.sSelectSize.ToString();
            //select.Controls.Add(option);

            int sizesCount = item.sizes.Length;
            for (int i = 0; i < sizesCount; i++)
            {
                HtmlGenericControl options = new HtmlGenericControl("option");
                options.Attributes["value"] = item.sizes[i];
                options.InnerText = item.sizes[i];
                select.Controls.Add(options);
            }

            panel.Controls.Add(select);
        }
        internal static void GenerateTrending(Panel panelTrending, int cols)
        {
            try
            {
                List<Item> items = Database.GetTrendingOffers(cols);
                double c = items.Count;
                int index = 0;
                double numRows = Math.Ceiling(c / cols);

                for (int i = 0; i < numRows; i++)
                {
                    HtmlGenericControl divRow = new HtmlGenericControl("div");
                    divRow.Attributes["class"] = "row";
                    panelTrending.Controls.Add(divRow);

                    while (index < cols * (i + 1))
                    {
                        if (items[index] != null)
                        {


                            HtmlGenericControl divCol = new HtmlGenericControl("div");
                            divCol.Attributes["class"] = "col-md";
                            divRow.Controls.Add(divCol);

                            HtmlGenericControl divItem = new HtmlGenericControl("div");
                            divItem.Attributes["class"] = "item";
                            divCol.Controls.Add(divItem);

                            HtmlGenericControl a = new HtmlGenericControl("a");
                            a.Attributes["href"] = "/offer.aspx?id=" + items[index].id;
                            a.Attributes["class"] = "item-a";
                            divItem.Controls.Add(a);

                            HtmlGenericControl divImages = new HtmlGenericControl("div");
                            divImages.Attributes["class"] = "item-image";
                            a.Controls.Add(divImages);

                            Debug.WriteLine(items[index].id + "/" + items[index].images[0]);

                            HtmlGenericControl img2 = new HtmlGenericControl("img");
                            img2.Attributes["src"] = "assets/" + items[index].id + "/" + items[index].images[0];
                            divImages.Controls.Add(img2);

                            HtmlGenericControl img = new HtmlGenericControl("img");
                            img.Attributes["src"] = "assets/" + items[index].id + "/" + items[index].images[1];
                            img.Attributes["class"] = "img-top";
                            divImages.Controls.Add(img);

                            HtmlGenericControl p1 = new HtmlGenericControl("p");
                            p1.Attributes["class"] = "h5";
                            p1.InnerText += Resources.Resource.lPrice + " ";
                            a.Controls.Add(p1);

                            if (items[index].price_new != 0)
                            {
                                HtmlGenericControl span1 = new HtmlGenericControl("span");
                                span1.Attributes["class"] = "text-danger";
                                int test = items[index].price_new;
                                span1.InnerText = "$" + test.ToString();
                                p1.Controls.Add(span1);

                                HtmlGenericControl span2 = new HtmlGenericControl("span");
                                span2.Attributes["class"] = "text-muted old_price";
                                int test2 = items[index].price;
                                span2.InnerText = "$" + test2.ToString();
                                p1.Controls.Add(span2);
                            }
                            else
                            {
                                HtmlGenericControl span1 = new HtmlGenericControl("span");
                                int test = items[index].price;
                                span1.InnerText = "$" + test.ToString();
                                p1.Controls.Add(span1);
                            }




                            HtmlGenericControl p2 = new HtmlGenericControl("p");
                            p2.Attributes["class"] = "h6";
                            p2.InnerText = items[index].name;
                            a.Controls.Add(p2);

                            index += 1;
                        }
                    }
                }
            }
            catch
            {
                Debug.WriteLine("Błąd przy generowaniu oferty");
            }
        }
        public static void GenerateSpecialOffer(Panel panelOffers, int cols)
        {
            try
            {
                List<Item> items = Database.GetSpecialOffers();
                double c = items.Count;
                int index = 0;
                double numRows = Math.Ceiling(c / cols);

                for (int i = 0; i < numRows; i++)
                {
                    HtmlGenericControl divRow = new HtmlGenericControl("div");
                    divRow.Attributes["class"] = "row";
                    panelOffers.Controls.Add(divRow);

                    while (index < cols * (i + 1))
                    {
                        if (items[index] != null)
                        {


                            HtmlGenericControl divCol = new HtmlGenericControl("div");
                            divCol.Attributes["class"] = "col-md";
                            divRow.Controls.Add(divCol);

                            HtmlGenericControl divItem = new HtmlGenericControl("div");
                            divItem.Attributes["class"] = "item";
                            divCol.Controls.Add(divItem);

                            HtmlGenericControl a = new HtmlGenericControl("a");
                            a.Attributes["href"] = "/offer.aspx?id=" + items[index].id;
                            a.Attributes["class"] = "item-a";
                            divItem.Controls.Add(a);

                            HtmlGenericControl divImages = new HtmlGenericControl("div");
                            divImages.Attributes["class"] = "item-image";
                            a.Controls.Add(divImages);

                            Debug.WriteLine(items[index].id + "/" + items[index].images[0]);

                            HtmlGenericControl img2 = new HtmlGenericControl("img");
                            img2.Attributes["src"] = "assets/" + items[index].id + "/" + items[index].images[0];
                            divImages.Controls.Add(img2);

                            HtmlGenericControl img = new HtmlGenericControl("img");
                            img.Attributes["src"] = "assets/" + items[index].id + "/" + items[index].images[1];
                            img.Attributes["class"] = "img-top";
                            divImages.Controls.Add(img);

                            HtmlGenericControl p1 = new HtmlGenericControl("p");
                            p1.Attributes["class"] = "h5";
                            p1.InnerText += Resources.Resource.lPrice + " ";
                            a.Controls.Add(p1);

                            if (items[index].price_new != 0)
                            {
                                HtmlGenericControl span1 = new HtmlGenericControl("span");
                                span1.Attributes["class"] = "text-danger";
                                int test = items[index].price_new;
                                span1.InnerText = "$" + test.ToString();
                                p1.Controls.Add(span1);

                                HtmlGenericControl span2 = new HtmlGenericControl("span");
                                span2.Attributes["class"] = "text-muted old_price";
                                int test2 = items[index].price;
                                span2.InnerText = "$" + test2.ToString();
                                p1.Controls.Add(span2);
                            }
                            else
                            {
                                HtmlGenericControl span1 = new HtmlGenericControl("span");
                                int test = items[index].price;
                                span1.InnerText = "$" + test.ToString();
                                p1.Controls.Add(span1);
                            }




                            HtmlGenericControl p2 = new HtmlGenericControl("p");
                            p2.Attributes["class"] = "h6";
                            p2.InnerText = items[index].name;
                            a.Controls.Add(p2);

                            index += 1;
                        }
                    }
                }
            }
            catch
            {
                Debug.WriteLine("Błąd przy generowaniu oferty");
            }
        }

        public static void GenerateOffers(Panel panelOffers, int cols, string sex, string category)
        {
            try
            {
                List<Item> items = Database.GetOffers(sex, category);
                double c = items.Count;
                int index = 0;
                double numRows = Math.Ceiling(c / cols);

                for (int i = 0; i < numRows; i++)
                {
                    HtmlGenericControl divRow = new HtmlGenericControl("div");
                    divRow.Attributes["class"] = "row";
                    panelOffers.Controls.Add(divRow);

                    while (index < cols * (i + 1))
                    {
                        if (items[index] != null)
                        {


                            HtmlGenericControl divCol = new HtmlGenericControl("div");
                            divCol.Attributes["class"] = "col-md";
                            divRow.Controls.Add(divCol);

                            HtmlGenericControl divItem = new HtmlGenericControl("div");
                            divItem.Attributes["class"] = "item";
                            divCol.Controls.Add(divItem);

                            HtmlGenericControl a = new HtmlGenericControl("a");
                            a.Attributes["href"] = "/offer.aspx?id=" + items[index].id;
                            a.Attributes["class"] = "item-a";
                            divItem.Controls.Add(a);

                            HtmlGenericControl divImages = new HtmlGenericControl("div");
                            divImages.Attributes["class"] = "item-image";
                            a.Controls.Add(divImages);

                            Debug.WriteLine(items[index].id + "/" + items[index].images[0]);

                            HtmlGenericControl img2 = new HtmlGenericControl("img");
                            img2.Attributes["src"] = "assets/" + items[index].id + "/" + items[index].images[0];
                            divImages.Controls.Add(img2);

                            HtmlGenericControl img = new HtmlGenericControl("img");
                            img.Attributes["src"] = "assets/" + items[index].id + "/" + items[index].images[1];
                            img.Attributes["class"] = "img-top";
                            divImages.Controls.Add(img);

                            HtmlGenericControl p1 = new HtmlGenericControl("p");
                            p1.Attributes["class"] = "h5";
                            p1.InnerText += Resources.Resource.lPrice + " ";
                            a.Controls.Add(p1);

                            if (items[index].price_new != 0)
                            {
                                HtmlGenericControl span1 = new HtmlGenericControl("span");
                                span1.Attributes["class"] = "text-danger";
                                int test = items[index].price_new;
                                span1.InnerText = "$" + test.ToString();
                                p1.Controls.Add(span1);

                                HtmlGenericControl span2 = new HtmlGenericControl("span");
                                span2.Attributes["class"] = "text-muted old_price";
                                int test2 = items[index].price;
                                span2.InnerText = "$" + test2.ToString();
                                p1.Controls.Add(span2);
                            }
                            else
                            {
                                HtmlGenericControl span1 = new HtmlGenericControl("span");
                                int test = items[index].price;
                                span1.InnerText = "$" + test.ToString();
                                p1.Controls.Add(span1);
                            }




                            HtmlGenericControl p2 = new HtmlGenericControl("p");
                            p2.Attributes["class"] = "h6";
                            p2.InnerText = items[index].name;
                            a.Controls.Add(p2);

                            index += 1;
                        }
                    }
                }
            }
            catch
            {
                Debug.WriteLine("Błąd przy generowaniu oferty");
            }
        }
    }
}