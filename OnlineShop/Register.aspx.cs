﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineShop
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                Response.Redirect("/");
            }
            else
            {
                Generator.GenerateLoginButtons(lLoginButtons);
            }
            Generator.GenerateDropdownCategory(PanelDropdownMale, Resources.Resource.lMale);
            Generator.GenerateDropdownCategory(PanelDropdownFemale, Resources.Resource.lFemale);
        }

        protected void lbtnLanguage_Click(object sender, EventArgs e)
        {
            string option = lbtnLanguage.Text.ToString();
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(option);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(option);
            Session["culture"] = option;
            Response.Redirect(Request.Url.LocalPath);
        }

        protected override void InitializeCulture()
        {
            if (Session["culture"] != null)
            {
                UICulture = Session["culture"].ToString();
                Culture = Session["culture"].ToString();
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Session["culture"].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["culture"].ToString());
            }
            base.InitializeCulture();
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            
            if (Page.IsValid)
            {
                int insert = OnlineShop.User.Register(tbUsername.Text.ToString(), tbEmail.Text.ToString(), tbPassword.Text.ToString());
                if (insert == 0)
                {
                    string message = Resources.Resource.registerProblem.ToString();
                    string alert = "<script language =\"javascript\" type=\"text/javascript\">alert('"+message+"');</script>";
                    Response.Write(alert);
                }
                else if(insert == -1)
                {
                    string message = Resources.Resource.databaseProblem.ToString();
                    string alert = "<script language =\"javascript\" type=\"text/javascript\">alert('" + message + "');</script>";
                    Response.Write(alert);
                }
                else
                {
                    Response.Redirect("login.aspx");
                }
            }
        }
    }
}