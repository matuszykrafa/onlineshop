﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace OnlineShop
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                Generator.GenerateHelloLabel(lLoginButtons, Session["user"].ToString());
            }
            else
            {
                Generator.GenerateLoginButtons(lLoginButtons);
            }
            Generator.GenerateDropdownCategory(PanelDropdownMale, Resources.Resource.lMale);
            Generator.GenerateDropdownCategory(PanelDropdownFemale, Resources.Resource.lFemale);
            Generator.GenerateSpecialOffer(panelSpecialOffer, 5);
            Generator.GenerateTrending(panelTrending, 5);
        }

        protected void lbtnLanguage_Click(object sender, EventArgs e)
        {
            string option = lbtnLanguage.Text.ToString();
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(option);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(option);
            Session["culture"] = option;
            Response.Redirect(Request.Url.PathAndQuery);
        }

        protected void lbtnCart_Click(object sender, EventArgs e)
        {
            
        }

        protected override void InitializeCulture()
        {
            if (Session["culture"] != null)
            {
                UICulture = Session["culture"].ToString();
                Culture = Session["culture"].ToString();
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Session["culture"].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["culture"].ToString());
            }
            base.InitializeCulture();
        }
    }
}