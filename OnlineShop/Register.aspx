﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="OnlineShop.Register" Culture="auto:en-US" UICulture="auto" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="assets/favicon.ico">
    <title>Clothesland</title>
    <script src="js/jquery-3.4.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body style="background-color:#f8f9fa">
    <form id="form1" runat="server">
        <div class="container-fluid website">
            <header class="container">
               <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
                        aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <img class="logo" src="assets/logo2.png" alt="" />Clothesland</a>

                    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li class="nav-item dropdown">
                                <asp:Panel runat="server" ID="PanelDropdownMale">
                                </asp:Panel>
                            </li>
                            <li class="nav-item dropdown">
                                <asp:Panel runat="server" ID="PanelDropdownFemale">
                                </asp:Panel>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/Contact.aspx">
                                    <asp:Label ID="Label10" runat="server" Text="<%$Resources: Resource, lContact %>"></asp:Label></a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <div class="dropdown-divider"></div>
                            </li>
                            <li>
                                <a class="nav-link" href="/Cart.aspx">
                                    <asp:Label ID="Label11" runat="server" Text="<%$Resources: Resource, lCart %>"></asp:Label></a>
                            </li>
                            <asp:Label ID="lLoginButtons" runat="server" CssClass="nav navbar-nav navbar-right"></asp:Label>
                            <li>
                                <div class="dropdown-divider"></div>
                            </li>
                            <li>
                                <asp:LinkButton ID="lbtnLanguage" runat="server" CssClass="nav-link" Text="<%$ Resources:Resource, lLang %>" OnClick="lbtnLanguage_Click" CausesValidation="False"></asp:LinkButton></li>
                        </ul>
                    </div>
                </nav>
            </header>
            <div id="content" class="container">
                <div class="container register-form">
                    <div class="form-group row">
                        <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, lUsername %>" CssClass="col-sm-2 col-form-label" ></asp:Label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="tbUsername" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="validatorUsername" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbUsername" CssClass="invalid-feedback2">
                                <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lInvalidUsername %>"></asp:Label>
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group row">
                        <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, lEmail %>" CssClass="col-sm-2 col-form-label"></asp:Label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="tbEmail" runat="server" TextMode="Email" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbEmail" CssClass="invalid-feedback2">
                                <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lInvalidEmail %>"></asp:Label>
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group row">
                        <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, lPassword %>" CssClass="col-sm-2 col-form-label"></asp:Label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="tbPassword" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbPassword" CssClass="invalid-feedback2">
                                <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lInvalidPassword %>"></asp:Label>
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group row">
                        <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, lRepeatPassword %>" CssClass="col-sm-2 col-form-label"></asp:Label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="tbPassword2" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="tbPassword2" CssClass="invalid-feedback2">
                                <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lInvalidPassword %>"></asp:Label>
                            </asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="CompareValidator" CssClass="invalid-feedback2" ControlToCompare="tbPassword" ControlToValidate="tbPassword2">
                                <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lInvalidPassword2 %>"></asp:Label>
                            </asp:CompareValidator>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <asp:Button ID="btnRegister" runat="server" Text="<%$ Resources:Resource, lRegister %>" CssClass="btn btn-primary" OnClick="btnRegister_Click"/>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="container">
                <p class="text-center">by Rafał Matuszyk</p>
            </footer>
        </div>
    </form>
</body>

</html>
