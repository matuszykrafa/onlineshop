﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace OnlineShop
{
    public class User
    {
        public string id { get; set; }
        public string user { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string salt { get; set; }
        public string type { get; set; }


        public static void Logout(object sender, EventArgs e)
        {
            System.Web.HttpContext.Current.Session["user"] = null;
            System.Web.HttpContext.Current.Session["user_type"] = null;
            System.Web.HttpContext.Current.Response.Redirect(System.Web.HttpContext.Current.Request.Url.ToString(), true);
        }

        public static int Register(string username, string email, string password)
        {
            User user = new User();
            user.user = username;
            user.email = email;
            user.password = password;
            HashPassword(user);

            return Database.Insert("INSERT INTO `users` (`id`, `login`, `email`, `hash`, `salt`, `type`) VALUES (NULL, '" + user.user + "', '" + user.email + "', '" + user.password + "', '" + user.salt + "', 'user')");
        }

        public static int ChangePassword(string username, string oldPassword, string newPassword)
        {
            dynamic user = Database.GetUser(username);
            try
            {
                var hash = new Rfc2898DeriveBytes(oldPassword, Convert.FromBase64String(user.salt), 10000).GetBytes(32);
                if (user.password == Convert.ToBase64String(hash))
                {
                    User newUser = new User();
                    newUser.user = user.user;
                    newUser.password = newPassword;
                    HashPassword(newUser);
                    return Database.ChangePassword(newUser);
                }
                else
                {
                    return 0;
                }
            }
            catch
            {
                return 0;
            }
        }

        private static void HashPassword(User user)
        {
            var salt = new byte[32];
            var hash = new byte[32];
            new RNGCryptoServiceProvider().GetBytes(salt);
            hash = new Rfc2898DeriveBytes(user.password, salt, 10000).GetBytes(32);
            user.password = Convert.ToBase64String(hash);
            user.salt = Convert.ToBase64String(salt);
        }
    }
}