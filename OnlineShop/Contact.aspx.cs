﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineShop
{
    public partial class Contact : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                Generator.GenerateHelloLabel(lLoginButtons, Session["user"].ToString());
                tbEmail.Text = Session["user_email"].ToString();
            }
            else
            {
                Generator.GenerateLoginButtons(lLoginButtons);
            }
            Generator.GenerateDropdownCategory(PanelDropdownMale, Resources.Resource.lMale);
            Generator.GenerateDropdownCategory(PanelDropdownFemale, Resources.Resource.lFemale);
        }

        protected void lbtnLanguage_Click(object sender, EventArgs e)
        {
            string option = lbtnLanguage.Text.ToString();
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(option);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(option);
            Session["culture"] = option;
            Response.Redirect(Request.Url.PathAndQuery);
        }

        protected void btnSendMessage_Click(object sender, EventArgs e)
        {
            MailMessage message = new MailMessage(tbEmail.Text.ToString(), "clothesland2@gmail.com");
            message.From = new MailAddress(tbEmail.Text.ToString(), tbEmail.Text.ToString());
            message.Subject = tbSubject.Text.ToString();
            message.Body = tbMessage.Text.ToString();
            SmtpClient client = new SmtpClient("smtp.gmail.com");
            client.UseDefaultCredentials = false;
            client.EnableSsl = true;
            client.Credentials = new System.Net.NetworkCredential("clothesland2@gmail.com", "mamatata1");
            
            try
            {
                client.Send(message);
            }
            catch
            {
                Debug.WriteLine("Błąd przy wysyłaniu maila");
            }
        }

        protected override void InitializeCulture()
        {
            if (Session["culture"] != null)
            {
                UICulture = Session["culture"].ToString();
                Culture = Session["culture"].ToString();
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Session["culture"].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["culture"].ToString());
            }
            base.InitializeCulture();
        }
    }
}