﻿//------------------------------------------------------------------------------
// <generowany automatycznie>
//     Ten kod został wygenerowany przez narzędzie.
//
//     Modyfikacje tego pliku mogą spowodować niewłaściwe zachowanie i zostaną utracone
//     w przypadku ponownego wygenerowania kodu. 
// </generowany automatycznie>
//------------------------------------------------------------------------------

namespace OnlineShop
{


    public partial class Default
    {

        /// <summary>
        /// Kontrolka form1.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;

        /// <summary>
        /// Kontrolka PanelDropdownMale.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel PanelDropdownMale;

        /// <summary>
        /// Kontrolka PanelDropdownFemale.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel PanelDropdownFemale;

        /// <summary>
        /// Kontrolka Label2.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label2;

        /// <summary>
        /// Kontrolka Label3.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label3;

        /// <summary>
        /// Kontrolka lLoginButtons.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lLoginButtons;

        /// <summary>
        /// Kontrolka lbtnLanguage.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton lbtnLanguage;

        /// <summary>
        /// Kontrolka lSpecialOffer.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lSpecialOffer;

        /// <summary>
        /// Kontrolka panelSpecialOffer.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel panelSpecialOffer;

        /// <summary>
        /// Kontrolka Label1.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label1;

        /// <summary>
        /// Kontrolka panelTrending.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel panelTrending;
    }
}
