﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineShop
{
    public class Item
    {
        public string id { get; set; }
        public string name { get; set; }
        public string category { get; set; }
        public string color { get; set; }
        public string[] sizes { get; set; }
        public string sizesString { get; set; }
        public int price { get; set; }
        public int price_new { get; set; }
        public int stock { get; set; }
        public string gender { get; set; }
        public string[] images { get; set; }
        public string imagesString { get; set; }
    }
}