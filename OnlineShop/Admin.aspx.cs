﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineShop
{
    public partial class Admin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                if (Session["user_type"].ToString() == "admin")
                {
                    Generator.GenerateHelloLabel(lLoginButtons, Session["user"].ToString());

                }
                else
                {
                    Response.Redirect("/");
                }
            }
            else
            {
                Response.Redirect("/Login.aspx");
            }
            Generator.GenerateDropdownCategory(PanelDropdownMale, Resources.Resource.lMale);
            Generator.GenerateDropdownCategory(PanelDropdownFemale, Resources.Resource.lFemale);
        }
        protected void lbtnLanguage_Click(object sender, EventArgs e)
        {
            string option = lbtnLanguage.Text.ToString();
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(option);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(option);
            Session["culture"] = option;
            Response.Redirect(Request.Url.PathAndQuery);
        }

        protected override void InitializeCulture()
        {
            if (Session["culture"] != null)
            {
                UICulture = Session["culture"].ToString();
                Culture = Session["culture"].ToString();
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Session["culture"].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["culture"].ToString());
            }
            base.InitializeCulture();
        }

        protected void btnAddCategory_Click(object sender, EventArgs e)
        {
            string name = tbCategoryName.Text.ToString();
            string namePL = tbCategoryNamePL.Text.ToString();
            int add = Database.AddCategory(name, namePL);
            string message = "";
            if (add == 0)
            {
                message = Resources.Resource.addCategoryProblem.ToString();
            }
            else if (add == -1)
            {
                message = Resources.Resource.databaseProblem.ToString();
            }
            else if (add == 1)
            {
                message = Resources.Resource.addCategorySuccess.ToString();
            }
            string alert = "<script language =\"javascript\" type=\"text/javascript\">alert('" + message + "');</script>";
            Response.Write(alert);
            Response.Redirect("/Admin.aspx");
        }

        protected void btnAddItem_Click(object sender, EventArgs e)
        {
            try
            {
                Item item = new Item();
                item.id = (Convert.ToInt32(Database.GetNewID()) + 1).ToString();
                item.name = tbItemName.Text.ToString();
                item.category = ddlCategories.SelectedValue.ToString();
                item.color = tbColor.Text.ToString();
                item.price = Convert.ToInt32(tbPrice.Text.ToString());
                item.price_new = Convert.ToInt32(tbPriceNew.Text.ToString());
                item.stock = Convert.ToInt32(tbInStock.Text.ToString());
                item.gender = rbListSex.SelectedValue.ToString();

                string size = "";
                List<ListItem> selected = new List<ListItem>();
                int i = 0;
                foreach (ListItem el in cbListSize.Items)
                {
                    if (el.Selected)
                    {
                        if (i == 0) size += el.Value.ToString();
                        else size += "," + el.Value.ToString();
                        i++;
                    }
                }
                if (size == "")
                {
                    size = "M";
                }
                item.sizesString = size;


                var folder = Server.MapPath("~/assets/" + item.id);
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                string imgs = "";
                int n = 0;
                foreach (HttpPostedFile file in fileUploadAddItems.PostedFiles)
                {
                    string filename = Path.GetFileName(file.FileName);
                    if (n == 0) imgs += filename;
                    else imgs += "," + filename;
                    n++;
                    file.SaveAs(Server.MapPath("~/assets/" + item.id + "/") + filename);
                }
                item.imagesString = imgs;
                Database.AddItem(item);

            }
            catch
            {
                Debug.WriteLine("ERROR przy dodawaniu itemu");
            }
        }

        protected void lbtnDeleteCategory_Click(object sender, EventArgs e)
        {
            if (listBoxCategories.SelectedItem != null)
            {
                int delete = Database.DeleteCategory(listBoxCategories.SelectedValue.ToString());
                if (delete == 1)
                {
                    Response.Redirect("/Admin.aspx");
                }
            }
        }

        protected void lbtnUpdateCategory_Click(object sender, EventArgs e)
        {
            if (listBoxCategories.SelectedItem != null)
            {
                int update = Database.UpdateCategory(listBoxCategories.SelectedValue.ToString(), tbUpdateCategory.Text.ToString(), tbUpdateCategoryPolish.Text.ToString());
                if (update == 1)
                {
                    Response.Redirect("/Admin.aspx");
                }
            }
        }

        protected void lbtnDeleteItem_Click(object sender, EventArgs e)
        {
            if (listBoxItems.SelectedItem != null)
            {
                int delete = Database.DeleteItem(listBoxItems.SelectedValue.ToString());
                if (delete == 1)
                {
                    Response.Redirect("/Admin.aspx");
                }
            }
        }

        protected void lbtnUpdateItem_Click(object sender, EventArgs e)
        {
            if (listBoxItems.SelectedItem != null)
            {
                try
                {
                    Item item = new Item();
                    item.id = listBoxItems.SelectedValue.ToString();
                    item.name = tbUpdateItemName.Text.ToString();
                    item.category = ddlUpdateItem.SelectedValue.ToString();
                    item.color = tbUpdateColor.Text.ToString();
                    item.price = Convert.ToInt32(tbUpdatePrice.Text.ToString());
                    item.price_new = Convert.ToInt32(tbUpdatePriceNew.Text.ToString());
                    item.stock = Convert.ToInt32(tbUpdateInStock.Text.ToString());
                    item.gender = rblUpdateItem.SelectedValue.ToString();

                    string size = "";
                    List<ListItem> selected = new List<ListItem>();
                    int i = 0;
                    foreach (ListItem el in cblUpdateItem.Items)
                    {
                        if (el.Selected)
                        {
                            if (i == 0) size += el.Value.ToString();
                            else size += "," + el.Value.ToString();
                            i++;
                        }
                    }
                    if (size == "")
                    {
                        size = "M";
                    }
                    item.sizesString = size;


                    var folder = Server.MapPath("~/assets/" + item.id);
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                    string imgs = "";
                    int n = 0;
                    foreach (HttpPostedFile file in fileUpdateUploadAddItems.PostedFiles)
                    {
                        string filename = Path.GetFileName(file.FileName);
                        if (n == 0) imgs += filename;
                        else imgs += "," + filename;
                        n++;
                        file.SaveAs(Server.MapPath("~/assets/" + item.id + "/") + filename);
                    }
                    item.imagesString = imgs;
                    int update = Database.UpdateItem(item);
                    if (update == 1)
                    {
                        Response.Redirect("/Admin.aspx");
                    }
                }
                catch
                {
                    Debug.WriteLine("ERROR przy edytowniu");
                }

            }
        }

        protected void lbtnDeleteUser_Click(object sender, EventArgs e)
        {
            if (listBoxUsers.SelectedItem != null)
            {
                Database.deleteUser(listBoxUsers.SelectedValue.ToString());

                if (listBoxUsers.SelectedValue.ToString() == Session["user_id"].ToString())
                {
                    System.Web.HttpContext.Current.Session["user"] = null;
                    System.Web.HttpContext.Current.Session["user_id"] = null;
                    System.Web.HttpContext.Current.Session["user_type"] = null;
                    System.Web.HttpContext.Current.Response.Redirect(System.Web.HttpContext.Current.Request.Url.ToString(), true);
                }
                Response.Redirect("/Admin.aspx");
            }
        }

        protected void lbtnMakeUser_Click(object sender, EventArgs e)
        {
            if (listBoxUsers.SelectedItem != null)
            {
                Database.changeUserType(listBoxUsers.SelectedValue.ToString(), "user");
                if (listBoxUsers.SelectedValue.ToString() == Session["user_id"].ToString()) Session["user_type"] = "user";
                Response.Redirect("/Admin.aspx");
            }
        }

        protected void lbtnMakeAdmin_Click(object sender, EventArgs e)
        {
            if (listBoxUsers.SelectedItem != null)
            {
                Database.changeUserType(listBoxUsers.SelectedValue.ToString(), "admin");

                if (listBoxUsers.SelectedValue.ToString() == Session["user_id"].ToString()) Session["user_type"] = "admin";
                Response.Redirect("/Admin.aspx");

            }
        }
        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int insert = OnlineShop.User.Register(tbLogin.Text.ToString(), tbEmaill.Text.ToString(), tbPassword5.Text.ToString());
                if (insert == 0)
                {
                    string message = Resources.Resource.registerProblem.ToString();
                    string alert = "<script language =\"javascript\" type=\"text/javascript\">alert('" + message + "');</script>";
                    Response.Write(alert);
                }
                else if (insert == -1)
                {
                    string message = Resources.Resource.databaseProblem.ToString();
                    string alert = "<script language =\"javascript\" type=\"text/javascript\">alert('" + message + "');</script>";
                    Response.Write(alert);
                }
                else
                {
                    Response.Redirect("/Admin.aspx");
                }
            }
        }

        protected void lbtnPlaced_Click(object sender, EventArgs e)
        {
            if (lbOrders.SelectedItem != null)
            {
                Database.changeOrderStatus(lbOrders.SelectedValue.ToString(), "Placed");
                changeStatusEmail(lbOrders.SelectedValue.ToString(), "Placed", Database.getEmailFromOrder(lbOrders.SelectedValue.ToString()));
                Response.Redirect("/Admin.aspx");
            }
        }

        protected void lbtnInProgress_Click(object sender, EventArgs e)
        {
            if (lbOrders.SelectedItem != null)
            {
                Database.changeOrderStatus(lbOrders.SelectedValue.ToString(), "InProgress");
                changeStatusEmail(lbOrders.SelectedValue.ToString(), "InProgress", Database.getEmailFromOrder(lbOrders.SelectedValue.ToString()));
                Response.Redirect("/Admin.aspx");
            }
        }

        protected void lbtnShipped_Click(object sender, EventArgs e)
        {
            if (lbOrders.SelectedItem != null)
            {
                Database.changeOrderStatus(lbOrders.SelectedValue.ToString(), "Shipped");
                changeStatusEmail(lbOrders.SelectedValue.ToString(), "Shipped", Database.getEmailFromOrder(lbOrders.SelectedValue.ToString()));
                Response.Redirect("/Admin.aspx");
            }
        }

        protected void lbtnCancelled_Click(object sender, EventArgs e)
        {
            if (lbOrders.SelectedItem != null)
            {
                Database.changeOrderStatus(lbOrders.SelectedValue.ToString(), "Cancelled");
                changeStatusEmail(lbOrders.SelectedValue.ToString(), "Cancelled", Database.getEmailFromOrder(lbOrders.SelectedValue.ToString()));
                Response.Redirect("/Admin.aspx");
                
            }
        }

        protected void changeStatusEmail(string id, string status, string email)
        {
            MailMessage message = new MailMessage("clothesland2@gmail.com", email);
            message.From = new MailAddress("clothesland2@gmail.com", "Clothesland");
            message.Subject = "Zmiana statusu zamówienia nr: " + id;
            message.Body = "Status zamównienia uległ zmianie. Aktualny stan: " + status;
            SmtpClient client = new SmtpClient("smtp.gmail.com");
            client.UseDefaultCredentials = false;
            client.EnableSsl = true;
            client.Credentials = new System.Net.NetworkCredential("clothesland2@gmail.com", "mamatata1");

            try
            {
                client.Send(message);
            }
            catch
            {
                Debug.WriteLine("Błąd przy wysyłaniu maila");
            }
        }

        protected void lbtnMakeSpecial_Click(object sender, EventArgs e)
        {
            if (listBoxItems.SelectedItem != null)
            {
                Database.changeSpecial(listBoxItems.SelectedValue.ToString(), "1");
                Response.Redirect("/Admin.aspx");

            }
        }

        protected void lbtnMakeNormal_Click(object sender, EventArgs e)
        {
            if (listBoxItems.SelectedItem != null)
            {
                Database.changeSpecial(listBoxItems.SelectedValue.ToString(), "0");
                Response.Redirect("/Admin.aspx");

            }
        }
    }
}